\contentsline {section}{\numberline {1} \phantom {}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1} \phantom {}Background}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2} \phantom {}Objective and Method}{3}{subsection.1.2}
\contentsline {section}{\numberline {2} \phantom {}Summary of Results}{4}{section.2}
\contentsline {subsection}{\numberline {2.1} \phantom {}Stub Axle Properties and Assumptions}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2} \phantom {}Static Loading Analysis}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3} \phantom {}Dynamic Loading Analysis}{9}{subsection.2.3}
\contentsline {section}{\numberline {3} \phantom {}Discussion}{11}{section.3}
\contentsline {section}{\numberline {4} \phantom {}Conclusion}{12}{section.4}
\contentsline {section}{\hbox to\@tempdima {5\hfil }{ References}}{13}{section*.13}
