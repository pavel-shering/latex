\contentsline {section}{\numberline {1} \phantom {}Introduction}{2}
\contentsline {subsection}{\numberline {1.1} \phantom {}Background}{2}
\contentsline {subsection}{\numberline {1.2} \phantom {}Objective and Method}{3}
\contentsline {section}{\numberline {2} \phantom {}Summary of Results}{4}
\contentsline {subsection}{\numberline {2.1} \phantom {}Stub Axle Properties and Assumptions}{4}
\contentsline {subsection}{\numberline {2.2} \phantom {}Static Loading Analysis}{4}
\contentsline {subsection}{\numberline {2.3} \phantom {}Dynamic Loading Analysis}{9}
\contentsline {section}{\numberline {3} \phantom {}Discussion}{11}
\contentsline {subsection}{\numberline {3.1} \phantom {}Part I}{11}
\contentsline {subsection}{\numberline {3.2} \phantom {}Part II}{11}
\contentsline {section}{\hbox to\@tempdima {4\hfil }{ References}}{13}
\contentsline {section}{\numberline {Appendix A} \phantom {Appendix }MATLAB Code}{14}
\contentsline {subsection}{\numberline {Appendix A.1} \phantom {Appendix }Main Script}{14}
\contentsline {subsection}{\numberline {Appendix A.2} \phantom {Appendix }Functions}{21}
\contentsline {section}{\numberline {Appendix B} \phantom {Appendix }Calculations}{22}
