\contentsline {section}{\numberline {1} \phantom {}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1} \phantom {}Background}{1}{subsection.1.1}
\contentsline {section}{\numberline {2} \phantom {}Analysis}{3}{section.2}
\contentsline {subsection}{\numberline {2.1} \phantom {}Assumptions}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2} \phantom {}Equation of Motion}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3} \phantom {}Natural Frequency and Damping Ratio of TMD}{5}{subsection.2.3}
\contentsline {section}{\numberline {3} \phantom {}Total Response}{7}{section.3}
\contentsline {subsection}{\numberline {3.1} \phantom {}Homogenous Solution}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2} \phantom {}Particular Solution}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3} \phantom {}Total Solution}{9}{subsection.3.3}
\contentsline {section}{\numberline {4} \phantom {}Transmitted Force}{12}{section.4}
