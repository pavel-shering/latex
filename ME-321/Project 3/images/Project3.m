close all;
clear all;
clc;

c = 2000000;
k = 111880000;
m = 660000;
L = 42;
g = 9.81;
t = 0:0.001:2*2*pi/12.941;

f = c * ( (-1.511)*exp(-1.511*t).*(3.211*cos(12.941*t)- 0.562*sin(12.941*t))...
    + exp(-1.511*t).*((-12.941)*(3.211)*sin(12.941*t) + (12.941)*(-0.562)*cos(12.941*t)) ...
    + 3.211*(12.941)*sin(12.941*t) + 0.188*(12.941)*cos(12.941*t)) ...
    + (k + m*g/L)*(exp(-1.511*t).*(3.211*cos(12.941*t) - 0.562*sin(12.941*t)) ...
    - 3.211*cos(12.941*t) + 0.188*sin(12.941*t))
figure(1)
set(gca,'FontSize',12);
plot(t, f);
hold on

title('TMD Transmitted Force into the support frame')
ylabel('Force (N)')
xlabel('Time (s)')
box off
t = 0:0.001:2*2*pi/12.941;

z = exp(-1.511*t).* (3.211*cos(12.941*t) - 0.562*sin(12.941*t)) - 3.211*cos(12.941*t) + 0.188*sin(12.941*t)
figure(2)
set(gca,'FontSize',12);
plot(t, z);
hold on

title('TMD Transient Total Response')
ylabel('Displacement    z = x - y (m)')
xlabel('Time (s)')
box off

t = 0:0.001:2*pi/12.941;
z2 = - 3.211*cos(12.941*t) + 0.188*sin(12.941*t)
figure(3)
set(gca,'FontSize',12);
plot(t, z2);
hold on

title('TMD Steady State Total Response')
ylabel('Displacement    z = x - y (m)')
xlabel('Time (s)')
box off

t = 0:0.001:2*2*pi/12.941;

f = c * (3.211*(12.941)*sin(12.941*t) + 0.188*(12.941)*cos(12.941*t)) ...
    + (k + m*g/L)*(- 3.211*cos(12.941*t) + 0.188*sin(12.941*t))
figure(4)
set(gca,'FontSize',12);
plot(t, f);
hold on

title('TMD Transmitted Force into the support frame')
ylabel('Force (N)')
xlabel('Time (s)')
box off