\contentsline {section}{\numberline {1} \phantom {}Problem Details}{1}{section.1}
\contentsline {section}{\numberline {2} \phantom {}Gear Design}{2}{section.2}
\contentsline {subsection}{\numberline {2.1} \phantom {}Gear A (pinion) \& B (gear)}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1} \phantom {}Bending Stress}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2} \phantom {}Contact Stress}{6}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2} \phantom {}Gear C (pinion) \& D (gear)}{8}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1} \phantom {}Bending Stress}{9}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2} \phantom {}Contact Stress}{12}{subsubsection.2.2.2}
\contentsline {section}{\numberline {3} \phantom {}Shaft Design}{15}{section.3}
\contentsline {subsection}{\numberline {3.1} \phantom {}Motor Shaft Force Analysis}{15}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2} \phantom {}Reducer Shaft (Shaft 1) Force Analysis}{16}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1} \phantom {}Reducer Shaft 1 Arrangement and Specifications}{18}{subsubsection.3.2.1}
\contentsline {subsection}{\numberline {3.3} \phantom {}Reducer Middle Shaft (Shaft 2) Force Analysis}{23}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1} \phantom {}Reducer Shaft 2 Arrangement and Specifications}{27}{subsubsection.3.3.1}
\contentsline {subsection}{\numberline {3.4} \phantom {}Output Reducer Shaft (Shaft 3) Force Analysis}{31}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1} \phantom {}Reducer Shaft 3 Arrangement and Specifications}{33}{subsubsection.3.4.1}
\contentsline {section}{\numberline {4} \phantom {}Conclusion}{40}{section.4}
\contentsline {section}{\hbox to\@tempdima {5\hfil }{ References}}{41}{section*.68}
