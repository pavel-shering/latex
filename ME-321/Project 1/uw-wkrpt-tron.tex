% uw-wkrpt-ece.tex - An example work report that uses uw-wkrpt.cls
% Copyright (C) 2002,2003  Simon Law
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% We begin by calling the workreport class which includes all the
% definitions for the macros we will use.
\documentclass[]{uw-wkrpt}

% We will use some packages to add functionality
\usepackage{graphicx} % Include graphic importing
\usepackage{gensymb}
\usepackage{amsmath}
\usepackage{fixltx2e}
\usepackage{array}
\usepackage{longtable}
\usepackage{enumitem}
\usepackage{caption}
\usepackage{tabularx}
\usepackage{pstricks}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}

\usepackage{hyperref}
\usepackage[all]{hypcap}  
\hypersetup{
    colorlinks = false,
    pageanchor = true
}

\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}


\captionsetup{
    format = plain,
    font = normalsize,
    labelfont = {bf}
}
\graphicspath{ {images/} }
\newcommand{\Mypm}{\mathbin{\tikz [x=1.4ex,y=1.4ex,line width=.1ex] \draw (0.0,0) -- (1.0,0) (0.5,0.08) -- (0.5,0.92) (0.0,0.5) -- (1.0,0.5);}}%
% Now we will begin writing the document.
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% IMPORTANT INFORMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% First we, should create a title page.  This is done below:
% Fill in the title of your report.
\title{Scaled Segway Stub Axle Safety Verification}

% Fill in your name.
\author{Pavel Shering}

% Fill in your student ID number.
\uwid{20523043}

% Fill in your home address.
\address{123 University Ave. W.,\\*
         Waterloo, ON\ \ N2L 3G1}

% Fill in your employer's name.
\employer{Project 1 \\ ME-321 - Kinematics and Dynamics of Machines}

% Fill in your employer's city and province.
\employeraddress{}

% Fill in your school's name.
\school{University of Waterloo}

% Fill in your faculty name.
\faculty{Faculty of Engineering}

% Fill in your e-mail address.
\email{Instructor: Eihab Abdel-Rahman}

% Fill in your term.
\term{3A}

% Fill in your program.
\program{Mechatronics Engineering}

% Fill in the department chair's name.
\chair{Dr.\ William \ Melek}

% Fill in the department chair's mailing address.
\chairaddress{Mechanical and Mechatronics Engineering Department,\\*
              University of Waterloo,\\*
	      Waterloo, ON\ \ N2L 3G1}

% If you are writing a "Confidential 1" report, uncomment the next line.
%\confidential{Confidential-1}

% If you want to specify the date, fill it in here.  If you comment out
% this line, today's date will be substituted.
\date{May 30, 2016}

% Now, we ask LaTeX to generate the title.
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FRONT MATTER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \frontmatter will make the \section commands ignore their numbering,
%% it will also use roman page numbers.
\frontmatter


% Next, we need to make a Table of Contents, List of Figures and 
% List of Tables.  You will most likely need to run LaTeX twice to
% get these correct.  The first pass for LaTeX to figure out the
% labels, and the second pass to put in the right references.
\tableofcontents
\listoffigures
\listoftables

% We continue with required sections, such as the Contributions and Summary
% \begin{onehalfspacing}
% \clearpage\section{Summary}


% \end{onehalfspacing}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REPORT BODY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \main will make the \section commands numbered again,
%% it will also use arabic page numbers.
\mainmatter

\section*{Abstract}\label{sec:abstract}

The purpose of this report is to analyze and verify the safety of a hollow SAE 1010 cold-drawn steel stub axle on a scaled down version of HT i180 Segway Cart designed by Chris McClellan. To verify the safety of the axle, a safety factor is calculated under static and dynamic loads.


The lowest factor of safety for static loading is 34.9484 due to shear stress. Additionally, the lowest factor of safety for dynamic loading is 9.6894, calculated by using Tresca Criterion. Both safety factors are calculated based on properties listed in Tables \ref{table:axle_propertries} and \ref{table:endurance_vals}. Thus, the axle design is limited by fatigue endured by dynamic loading.

\section{Introduction}\label{sec:intro}

\subsection{Background}

Dean Kamen designed a one person dynamically self-balancing transportation vehicle, Segway Personal Transporter (Figure \ref{fig:hti180}). Its meant to be yet another solution to green-energy transportation and revolutionize the way that short distance travel is conducted. The Segway PT costs \$0.50 of electricity for a full day of transportation, which is much more efficient than a traditional vehicle. The motion of the Segway is controlled by the rider shifting their weight forward to backward, and using the handle bar to turn.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.7]{images/ht_i180_segway.jpg}
    \caption{Model HT i180 Segway}
    \label{fig:hti180}
\end{figure}

A Mechatronics Engineer, Chris McClellan, at the University of Waterloo decided to scale down the HT i180 model to two thirds of the original to create an apparatus for demonstrations or labs as application of control theory. The scaled Segway is to hold a maximum payload of 35.66kg, compared to full size that holds 117kg. The drive system in the scaled model consists of Brushless DC motors (HBL-12" R-36), and wheels with internal stub axle.

\subsection{Objective and Method}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{images/drive_unit.png}
    \caption{Drive Unit Schematic illustrating the stub axle}
    \label{fig:drive_unit}
\end{figure}


\section{Summary of Results}\label{sec:results}

\subsection{Stub Axle Properties and Assumptions}

Table \ref{table:axle_propertries} displays the properties of the stub axle in the scaled model of the Segway that will be used in calculations of the safety factors.

\begin {table}[ht]
\caption {Scaled Segway Stub Axel Properties} \label{table:axle_propertries} 
\vspace{-0.5cm}

    \begin{center}
    \begin{tabular}{||c c||} 
    \hline
    Outer Diameter & 14.75mm \\
    \hline
    Inner Diameter & 7.25mm \\
    \hline
    Material & SAE 1010 cold-drawn steel\\
    \hline
    Yield Strength $\sigma_{y}$ & 305 MPa \\
    \hline
    Ultimate Strength $\sigma_{u}$ & 365 MPa \\
    \hline 
    \end{tabular}
    \end{center}
    \vspace{-0.3cm}
\end{table}

The calculations are performed under multiple underlying assumptions:
\begin{enumerate}[noitemsep]
\item Assume rigid body dynamics for static loading
\item Assume infinite service life of components for dynamic loads.
\item The effects of seal grooves and bearing seats on the axle are ignored
\item The force applied by the wheel and bearings on the axle are point forces, instead of distributed
\item Stresses due to torque induced by the motor are ignored
\item The force of the coupler is ignored
\item The mass of the platform and the max payload are evenly distributed between the two axles
\item The stub axle is wrought
\end{enumerate}

\subsection{Static Loading Analysis}

Figure \ref{fig:loading} illustrates the force loading and the dimensions of the axle. A and B are the bearing that the axle goes through and C is the wheel with the internal stub axle under analysis.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.5]{images/static.png}
    \caption{Loading diagram of the stub axle}
    \label{fig:loading}
\end{figure}

First, calculate the force applied on the wheel using half of the combined mass of the platform and the maximum payload of 35.66 kg.

\begin{equation} \label{eq:Cy}
    \begin{aligned}
        C_{Y} &= \frac{\left( m_{platform} + m_{MaxPayload} \right) *g}{2} \\
              &= \frac{\left( 30.9 \text{ kg} + 35.66 \text{ kg} \right )\left( 9.81 \text{ m/$s^{2}$} \right)}{2} \\
              &= 326.4768 \text{ N} \\
    \end{aligned}
\end{equation} 

Next, to find the force that bearing A exerts on the axle, about bearing B the moments are summed.


\begin{equation} \label{eq:Ay}
    \begin{aligned}
        \sum{M_{@ B}} &= 0 \\
             0 &= A_{Y}\Delta X_{AB} - B_{Y}\Delta X_{BC} \\
            A_{Y} &= B_{Y} \frac{\Delta X_{BC}}{\Delta X_{AB}} \\
            A_{Y} &= 326.4768 \text{ N}\frac{6.5 \text{ mm}}{75.5 \text{ mm}} \\
            A_{Y} &= 28.1073 \text{ N} \\
    \end{aligned}
\end{equation} 

Lastly, by the force equilibrium in the Y direction find $B_{Y}$.

\begin{equation} \label{eq:By}
    \begin{aligned}
        \sum{F_{Y}} &= 0 \\
             0 &= A_{Y} - B_{Y} + C_{Y} \\
            B_{Y} &=  A_{Y} + C_{Y}\\
            B_{Y} &= 354.5841 \text{ N} \\
    \end{aligned}
\end{equation} 

Using the calculated forces the shear (Figure \ref{fig:shear_diagram}) and bending moment (Figure \ref{fig:moment_diagram}) diagrams are created.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.45]{images/shear_diagram.png}
    \caption{Shear force diagram over axle's length}
    \label{fig:shear_diagram}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.45]{images/moment_diagram.png}
    \caption{Bending moment diagram over axle's length}
    \label{fig:moment_diagram}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.8]{images/stress_circle.png}
    \caption{Stress location on the cross section at B}
    \label{fig:stress_circle}
\end{figure}

As seen from the graphs above the maximum shear force of \emph{V = 326.4768 N} occurs from bearing \emph{B} to the wheel (\emph{C}), which is used to calculate maximum shear force that occurs at the neutral axis (Figure \ref{fig:stress_circle}).

\begin{equation} \label{eq:shear}
    \begin{aligned}
        \tau_{B} &= \frac{VQ}{It} \\
            &\approx \frac{2V}{A} \text{ , A is the cross sectional area of the beam}\\
            &\approx \frac{2V}{\pi \left(r_{out}^{2} - r_{in}^{2}\right)}\\
            &\approx \frac{2 \left(326.4768 \text{ N}\right)}{\pi \left(\frac{14.75\text{ mm}}{2}^{2} - \frac{7.25\text{ mm}}{2}^{2}\right)}\\
            &\approx 5.0386 \text{ MPa}\\
    \end{aligned}
\end{equation} 


Additionally, the maximum bending moment of \emph{ M = 2.1221 Nm} occurs at point \emph{B}, which is used to calculate the maximum stress due to bending that occurs at top and bottom of the axle (max distance away from neutral axis \emph{y}) as illustrated in Figure \ref{fig:stress_circle}.

\begin{equation} \label{eq:tensile}
    \begin{aligned}
        \sigma_{B} &= \frac{My}{I} \\
              &= \frac{My}{\frac{\pi}{4}\left(r_{out}^{2} - r_{in}^{2}\right)} \\
            &=  \frac{(2.1221 \text{ Nm})*(1000)(\frac{14.75 \text{ mm}}{2})}{\frac{\pi}{4}\left(\frac{14.75 \text{ mm}}{2000}^{2} - \frac{7.25 \text{ mm}}{2000}^{2}\right)}\\
            &= 7.1533 \text{ MPa}\\
    \end{aligned}
\end{equation} 

The two stress elements are considered to determine principle stresses using Von Mises method. As mentioned above, the maximum axial stress occurs at maximum distance from the neutral axis at the top and bottom of the axle. Therefore, in this scenario there is only axial stress in the \emph{x} direction with no shear force present. Thus, the maximum principle stress is equal to stress due to bending, $\sigma_{B}$.

\newcommand{\rpm}{\sbox0{$1$}\sbox2{$\scriptstyle\pm$}
  \raise\dimexpr(\ht0-\ht2)/2\relax\box2 }

\begin{equation} \label{eq:principle_axial}
    \begin{aligned}
        \sigma_{\left(max,min\right)} &= \frac{\sigma_{x} + \sigma_{y}}{2} \rpm \sqrtsign{\left( \frac{\sigma_{x} + \sigma{y}}{2} \right)^2 + \tau_{xy}^{2} }\\
            &=  \frac{\sigma_{x} + 0}{2} \rpm \sqrtsign{\left( \frac{\sigma_{x} + 0}{2} \right)^2 + 0}\\
            &= \sigma_{B} \text{ , shown in Equation \ref{eq:tensile}}
    \end{aligned}
\end{equation} 

Using the Von Mises yield criterion to calculate the safety factor assuming rigid body dynamics.

\begin{equation} \label{eq:SFaxial}
    \begin{aligned}
        SF &= \frac{\sigma_{y}}{\sigma_{max}} \\
           &= \frac{\sigma_{y}}{\sigma_{B}} \\
           &= \frac{305 \text{ MPa}}{7.1533 \text{ MPa}} \\
           &= 42.6374
    \end{aligned}
\end{equation} 

The second stress element lies on the neutral axis, at which point the shear is maximum and no axial stress is present. 

\begin{equation} \label{eq:principle_shear}
    \begin{aligned}
        \sigma_{\left(max,min\right)} &= \frac{\sigma_{x} + \sigma_{y}}{2} \rpm \sqrtsign{\left( \frac{\sigma_{x} + \sigma{y}}{2} \right)^2 + \tau_{xy}^{2} }\\
            &=  \frac{0 + 0}{2} \rpm \sqrtsign{\left( \frac{0 + 0}{2} \right)^2 + \tau_{B}^{2}}\\
            &= \tau_{B} \text{ , shown in Equation \ref{eq:shear}}
    \end{aligned}
\end{equation} 

Again using the Von Mises yeild criterion which states that the magnitude of the shear stress in pure shear is $\sqrt{3}$ times lower than the tensile stress in the case of simple tension.

\begin{equation} \label{eq:SFshear}
    \begin{aligned}
        SF &= \frac{\sigma_{y}}{\sigma_{max}} \\
           &= \frac{\sigma_{y}}{\sqrt{3}\tau_{B}} \\
           &= \frac{305 \text{ MPa}}{\sqrt{3} * 5.0368 \text{ MPa}} \\
           &= 34.9486 \text{MPa}
    \end{aligned}
\end{equation} 

Therefore shear is the dominant method of static loading for the stub axle.

\subsection{Dynamic Loading Analysis}

The fatigue of the axle is due to the rotating shaft which is endures repeated and reversed stress. This creates a cyclic load of zero mean, and maximum amplitude of $\sigma_{a}$ = 7.1533 MPa and $\tau_{a}$ = 5.0386. The endurance limit for wrought cold-drawn steel with $\sigma_{u}$ = 365 MPa is approximately $s_{n}$ = 140 MPa from Figure 5-8 on page 172~\cite{ref:textbook}. To calculate the actual endurance limit Equation \ref{eq:endurance} is used.

\begin{equation} \label{eq:endurance}
    \begin{aligned}
        s_{n}' &= s_{n}C_{m}C_{st}C_{R}C_{s}
    \end{aligned}
\end{equation} 

Table \ref{table:endurance_vals} summarizes the axle material properties for calculating the endurance limit.

\begin {table}[ht]
\caption{Axle Endurance Material Properties} \label{table:endurance_vals} 
\vspace{-0.5cm}

    \begin{center}
    \begin{tabular}{||c c c||} 
    \hline
    $C_{m}$ & 1.0 & flaws in manufacturing in wrought steel \\
    \hline
    $C_{st}$ & 1.0 & bending stress \\
    \hline
    $C_{R}$ & 0.75 & reliability factor for 99.9\% reliability Table 5-2~\cite{ref:textbook} \\
    \hline
    $C_{s}$ & 7.62 $<$ D $\leq$ 50 & D = 14/75, size factor, Table 5-3~\cite{ref:textbook} Equation \ref{eq:Cs}\\
    \hline 
    \end{tabular}
    \end{center}
    \vspace{-0.3cm}
\end{table}

\begin{equation} \label{eq:Cs}
    \begin{aligned}
        C_{s} &= (\frac{D}{7.62 \text{ mm}})^-0.11 \\
              &= (\frac{14.75}{7.62 \text{ mm}})^-0.11 \\
              &= 0.9299
    \end{aligned}
\end{equation} 

Thus using Equation \ref{eq:endurance} above the actual endurance limit is calculated below (Equation \ref{eq:endurance_actual}.

\begin{equation} \label{eq:endurance_actual}
    \begin{aligned}
        s_{n}' &= \left(140 \text{ MPa}\right) \left(1.0\right)\left(1.0\right)\left(0.75\right)\left(0.9299\right) \\
        &= 97.6421 \text{ MPa}
    \end{aligned}
\end{equation} 

The principle stress $\sigma_{a}'$ is equal to $\sigma_{a}$ due to only axial stress at max distance away from the neutral axis, which causes the bending of the axle. Von Mises Criterion is then used to calculate the safety factor for zero mean dynamic load.

\begin{equation} \label{eq:final_SF_axial}
    \begin{aligned}
        K_{t}\sigma_{a}' &= \frac{s_{n}'}{N} \text{ , assuming no stress concentrations $K_{t}$ = 1}\\
              &= \frac{97.6421 \text{ MPa}}{7.1533 \text{ MPa}} \\
              &= 13.6499
    \end{aligned}
\end{equation} 

The principle stress $\tau_{a}'$ is equal to $\tau_{a}$ as shown before. Tresca Criterion is then used to calculate the safety factor for zero mean dynamic load.

\begin{equation} \label{eq:final_SF_shear}
    \begin{aligned}
        K_{t}\tau_{a}' &= \frac{s_{n}'}{2N} \text{ , assuming no stress concentrations $K_{t}$ = 1}\\
              &= \frac{97.6421 \text{ MPa}}{2 * 5.0386 \text{ MPa}} \\
              &= 9.6894
    \end{aligned}
\end{equation} 

\section{Discussion}\label{sec:discussion}

In reality the safety factor for both dynamic and static loads will be lower as the assumptions ignore torque and assume perfect manufacturing process which omits all stress concentrations of seal grooves and bearing seats. However, considering all the assumptions, the axle can be evaluated as over designed and will not be failing component of the Scaled Segway design. 

Furthermore, the verification determined that the stub axle will fail under dynamic loading conditions rather than static, as shown by the 3/4 decrease of the safety factor from static loading analysis.

Lastly, it was assumed that the principle stresses will occur at either the neutral axis or at the top and bottom (max distance away from the neutral axis). However, it is possible that there exists a combination of the two stresses that creates a greater maximum principle stress resulting in lower safety factor.


\section{Conclusion}\label{sec:conclusion}

The factor of safety for static loading of the scaled Segway SAE 1010 cold-drawn steel stub axle is 34.9484 and fails due to shear stress. The factor of safety for dynamic loading is 9.6894, calculated by using Tresca Criterion. Therefore the axle design is limited by fatigue endured by dynamic loading. The safety factors reflect correct results, based on the assumptions made before the calculations were done. Assuming all the assumptions are realistic and correct the conclusion to the safety verification is that the axle is over designed and can be reduced in weight to have a lower safety factor to save cost.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BACK MATTER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \backmatter will make the \section commands ignore their numbering,
\backmatter

% Here, we insert a References section, which will be formatted properly.
% The list of works you have referenced should be in FILENAME.bib,
% which will be workreport-sample.bib, if you use the command below.
%
% Note, you will need to process the document in a certain order.  First,
% run LaTeX.  The % first pass will allow LaTeX to build a list of 
% references, it may % emit warning messages such as:
%   LaTeX Warning: Reference `app:gnugpl' on page 4 undefined on input line 277.
%   LaTeX Warning: There were undefined references.
% This is normal.  Now you run BiBTeX in order to generate the proper
% layout for the references.  After this, you run LaTeX once more.
\bibliography{uw-wkrpt-bib}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% APPENDICES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \appendix will reset \section numbers and turn them into letters.
%%
%% Don't forget to refer to all your appendices in the main report.

\appendix    
\end{document}