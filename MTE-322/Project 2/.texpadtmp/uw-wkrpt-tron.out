\BOOKMARK [1][-]{Doc-Start}{Introduction}{}% 1
\BOOKMARK [1][-]{section.1}{ FBD and Force Analysis}{}% 2
\BOOKMARK [1][-]{section.2}{ Shaft Design}{}% 3
\BOOKMARK [2][-]{subsection.2.1}{ Design by Fatigue Failure}{section.2}% 4
\BOOKMARK [2][-]{subsection.2.2}{ Design by Rigidity \(Deflection\)}{section.2}% 5
\BOOKMARK [2][-]{subsection.2.3}{ Design by Vibration}{section.2}% 6
\BOOKMARK [2][-]{subsection.2.4}{ Final Shaft Diameters}{section.2}% 7
\BOOKMARK [1][-]{section.3}{ Bearing Selection}{}% 8
\BOOKMARK [2][-]{subsection.3.1}{ Type of Bearing}{section.3}% 9
\BOOKMARK [2][-]{subsection.3.2}{ Bearing Life}{section.3}% 10
\BOOKMARK [2][-]{subsection.3.3}{ Bearing Inner Diameter}{section.3}% 11
\BOOKMARK [2][-]{subsection.3.4}{ Bearing Outer Diameter}{section.3}% 12
\BOOKMARK [2][-]{subsection.3.5}{ Bearing Arrangement and Fit}{section.3}% 13
\BOOKMARK [2][-]{subsection.3.6}{ Lubrication}{section.3}% 14
\BOOKMARK [2][-]{subsection.3.7}{ Sealing}{section.3}% 15
\BOOKMARK [2][-]{subsection.3.8}{ Final Design}{section.3}% 16
\BOOKMARK [1][-]{section.4}{ Detailed Designs}{}% 17
\BOOKMARK [2][-]{subsection.4.1}{ Shaft and Housing Design}{section.4}% 18
\BOOKMARK [2][-]{subsection.4.2}{ Re-Evaluate Correction Factors}{section.4}% 19
\BOOKMARK [1][-]{section.5}{ Assembly / Disassembly Procedure}{}% 20
\BOOKMARK [2][-]{subsection.5.1}{ Housing and Shaft Assembly}{section.5}% 21
\BOOKMARK [2][-]{subsection.5.2}{ Housing and Shaft Disassembly}{section.5}% 22
\BOOKMARK [1][-]{section.6}{ Group Member Contributions}{}% 23
\BOOKMARK [1][-]{section.7}{ Detailed Drawings}{}% 24
\BOOKMARK [1][-]{section*.26}{ References}{}% 25
\BOOKMARK [1][-]{appendix.A}{ Shear / Moment / Deflection Angle / Deflection Distance Diagrams}{}% 26
\BOOKMARK [1][-]{appendix.B}{ Main MATLAB Script}{}% 27
\BOOKMARK [1][-]{appendix.C}{ Item 1: Computation of Forces, Moments, Deflection Angle and Distance}{}% 28
\BOOKMARK [1][-]{appendix.D}{ Item 2: Fatigue, Deflection, and Vibration Calculations}{}% 29
\BOOKMARK [1][-]{appendix.E}{ Item 3: Static and Dynamic Analysis Calculations}{}% 30
