\contentsline {section}{\numberline {1} \phantom {}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2} \phantom {}Analysis}{2}{section.2}
\contentsline {subsection}{\numberline {2.1} \phantom {}Assumptions and Simplifications}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2} \phantom {}Control Volumes and Calculations}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1} \phantom {}Thermodynamic Analysis}{2}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2} \phantom {}Mechanics Analysis}{7}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3} \phantom {}Variable Ambient Temperature}{10}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4} \phantom {}Variable Pressure Ratio}{10}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5} \phantom {}Variable Effectiveness of the Air Cooler}{10}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6} \phantom {}Discussion and Recommendations}{15}{subsection.2.6}
