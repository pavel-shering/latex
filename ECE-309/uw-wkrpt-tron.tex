% uw-wkrpt-ece.tex - An example work report that uses uw-wkrpt.cls
% Copyright (C) 2002,2003  Simon Law
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% We begin by calling the workreport class which includes all the
% definitions for the macros we will use.
\documentclass[]{uw-wkrpt}

% We will use some packages to add functionality
\usepackage{graphicx} % Include graphic importing
\usepackage{gensymb}
\usepackage[]{amsmath}
\usepackage{fixltx2e}
\usepackage{array}
\usepackage{longtable}
\usepackage{caption}
\usepackage{tabularx}
\usepackage{pstricks}
\usepackage{listings}
\usepackage{mathtools}
\usepackage{float}
\usepackage{enumitem}

\usepackage{hyperref}
\usepackage[all]{hypcap}  
\hypersetup{
    colorlinks = false,
    pageanchor = true
}

\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}

\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}

\captionsetup{
    format = plain,
    font = normalsize,
    labelfont = {bf}
}
\graphicspath{ {images/} }
\newcommand{\Mypm}{\mathbin{\tikz [x=1.4ex,y=1.4ex,line width=.1ex] \draw (0.0,0) -- (1.0,0) (0.5,0.08) -- (0.5,0.92) (0.0,0.5) -- (1.0,0.5);}}%
% Now we will begin writing the document.
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% IMPORTANT INFORMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% First we, should create a title page.  This is done below:
% Fill in the title of your report.
\title{Compressed Air Car}

% Fill in your name.
\author{Pavel Shering}

% Fill in your student ID number.
\uwid{20523043}

% Fill in your home address.
\address{123 University Ave. W.,\\*
         Waterloo, ON\ \ N2L 3G1}

% Fill in your employer's name.
\employer{Project 1 \\ MTE 309 - Introduction to Thermodynamics and Heat Transfer}

% Fill in your employer's city and province.
\employeraddress{}

% Fill in your school's name.
\school{University of Waterloo}

% Fill in your faculty name.
\faculty{Faculty of Engineering}

% Fill in your e-mail address.
\email{Instructor: Dr. Richard Culham}

% Fill in your term.
\term{3A}

% Fill in your program.
\program{Mechatronics Engineering}

% Fill in the department chair's name.
\chair{Dr.\ William \ Melek}

% Fill in the department chair's mailing address.
\chairaddress{Mechanical and Mechatronics Engineering Department,\\*
              University of Waterloo,\\*
	      Waterloo, ON\ \ N2L 3G1}

% If you are writing a "Confidential 1" report, uncomment the next line.
%\confidential{Confidential-1}

% If you want to specify the date, fill it in here.  If you comment out
% this line, today's date will be substituted.
\date{July 21, 2016}

% Now, we ask LaTeX to generate the title.
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FRONT MATTER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \frontmatter will make the \section commands ignore their numbering,
%% it will also use roman page numbers.
\frontmatter


% Next, we need to make a Table of Contents, List of Figures and 
% List of Tables.  You will most likely need to run LaTeX twice to
% get these correct.  The first pass for LaTeX to figure out the
% labels, and the second pass to put in the right references.
\tableofcontents
\listoffigures
\listoftables

% We continue with required sections, such as the Contributions and Summary
% \begin{onehalfspacing}
% \clearpage\section{Summary}


% \end{onehalfspacing}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REPORT BODY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \main will make the \section commands numbered again,
%% it will also use arabic page numbers.
\mainmatter

\section{Introduction}\label{sec:intro}

Tata Motors developed an environmentally friendly compressed air powered AirPod (Figure~\ref{fig:airpod}). The vehicle is powered by the expansion of compressed air that moves from the storage tank and drives the air turbines located at the four wheels (Figure~\ref{fig:schematic}).

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.5]{images/airpod.png}
    \caption{Tata Motors AirPod}
    \label{fig:airpod}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.45]{images/schematic.png}
    \caption{AirPod charging and operation schematic}
    \label{fig:schematic}
\end{figure}

The AirPod's advantage is the low cost, light weight and being environmentally friendly. The total weight of the vehicle is 200 kg of which 24 kg is the engine. The operation of the car is simple, compressed air powers the turbines until the air in the tank reaches equilibrium with the surroundings. 

The main disadvantages of the AirPod is its speed, range, carry load and pressurized container. The maximum speed of the vehicle is 70 km/hr, and range varies from 120 - 150 km. To recharge the vehicle a specialized high pressure station is required. Lastly, vehicle's range depends on the load of the car thus making the transportation of anything besides the driver inefficient.

\section{Analysis}\label{sec:analysis}

\subsection{Assumptions and Simplifications}\label{sec:assume}

\begin{itemize}[leftmargin=*, noitemsep]
\item Negligible mechanical losses
\item Air is an idea gas
\item Negligible pressure losses in pipes, joints and the air-cooler
\item Specific heat is a function of temperature.
\item Assume correlations in Table A-2 are valid over full range of temperature
\item Time to reach maximum velocity is independent of total mass
\item When the air tank reaches atmospheric pressure the vehicle stops immediately, thus no deceleration. 
\item During the acceleration phase the mass of the vehicle remains constant
\end{itemize}

The assumptions made in this report are to simplify calculations as well as be able to evaluate the ideal case of the car design and determine the vehicles maximum performance. 

\subsection{Control Volumes and Calculations}\label{sec:control_and_calc}

\subsubsection{Thermodynamic Analysis}\label{thermo}
The calculation for thermodynamics of the AirCar are done based on the initial conditions shown in Table~\ref{table:initial_conditions}.

\begin{table}[ht]
    \newcolumntype{s}{>{\centering\arraybackslash}>{}X}
    \caption{Initial Conditions}%
    \arraybackslash%
    \begin{tabularx}{\textwidth}{X X c}%
    \textbf{Properties} & \textbf{} & \textbf{} \\%
    \hline  \\
    Ambient Temperature &($T_{1}$) &  20\degree\\
    Ambient Pressure &($P_{1}$) & 100 kPa \\
    Pressure Ratio &($r_{p}$) & 300  \\
    Polytropic Coefficient &(n) & 1.3 \\
    Volume of the air tank &(V) & 500 L \\
    Air gas constant &($R_{AIR}$) & 0.2870 $\frac{kJ}{kg \cdot K}$ \\
    Effectiveness of the cooler & ($\epsilon_{ac}$) & 0.5 \\
    \end{tabularx}    
    \label{table:initial_conditions} 
\end{table}

The amount of specific work required to fill the 500L air tank is calculated below (Equation~\ref{eq:Wcomp_specific}).

\begin{equation} \label{eq:Wcomp_specific}
    \begin{aligned}
        w_{comp} &= \frac{nRT_{1}}{n - 1}\left[\left(\frac{P_{2}}{P_{1}}\right)^{\frac{n-1}{n}}-1\right] \\
                 &= \frac{nRT_{1}}{n - 1}\left[\left(r_{p}\right)^{\frac{n-1}{n}}-1\right] \\
%                 &= \frac{(1.3)(0.2870\frac{kJ}{kg \cdot K})(20 + 273.15 K)}{1.3 - 1}\left[(300)^{\frac{1.3-1}{1.3}}-1\right] \\
                 &= 995.1008383 \text{ $\frac{kJ}{kg}$}   \\
    \end{aligned}
\end{equation} 

The air temperature after compression $T_{2}$ is required to find the temperature in the air tank $T_{3}$.

\begin{equation} \label{eq:T_2}
    \begin{aligned}
        T_{2} &= T_{1}\left(\frac{P_{2}}{P_{1}}\right)^{\frac{n-1}{n}} \\
%              &= (293.15 K)(300)^{\frac{1.3-1}{1.3}} \\
              &= 1093.284686 \text{ K}
    \end{aligned}
\end{equation} 

Solving for temperature in the air tank that is used to solve for total mass of the tank when its full ($m_{FULL}$) in Equation~\ref{eq:m_full}.

\begin{equation} \label{eq:T_3}
    \begin{aligned}
        T_{3} &= \left[1-\epsilon_{ac}\right](T_{2}-T_{1}) + T_{1} \\
%              &= \left[1-0.5\right](1093.284686-293.15) + 293.15 \\
              &= 693.2173432 \text{ K} \\
    \end{aligned}
\end{equation} 

Solving for $m_{FULL}$ at $T_{3}$ and $P_{3}$.

\begin{equation} \label{eq:m_full}
    \begin{aligned}
        m_{full} &= \frac{P_{3}{V}}{RT_{3}} \\
                 &= \frac{r_{p}P_{1}{V}}{RT_{3}} \\
%                 &= \frac{(300)(100 \text{ kPa}) (500 L \cdot \frac{1 m^3}{1000 L})}{(0.2870 \frac{kJ}{kg \cdot K}) (693.2173432 K)} \\
                 &= 75.39454815 \text{ kg} \\
    \end{aligned}
\end{equation} 

Finally solving for the work done by the compressor to fill the tank in Equation~\ref{eq:W_comp}.

\begin{equation} \label{eq:W_comp}
    \begin{aligned}
        W_{comp} &= w_{comp}m_{FULL} \\
%                 &= (995.1008383 \text{ $\frac{kJ}{kg}$})( 75.39454815 \text{ kg})   \\
                 &= 75025.17807 \text{ kJ}\\
    \end{aligned}
\end{equation} 

Determining the heat transfer from the air cooler $Q_{ac}$ using Equation~\ref{eq:Q_ac} below and the charging process control volume shown in Figure~\ref{fig:control_vol_charge}.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{images/charging_control_vol.png}
    \caption{Control volume of the charging processes}
    \label{fig:control_vol_charge}
\end{figure}

\begin{equation} \label{eq:Q_ac}
    \begin{aligned}
        \Delta U &= E_{IN} - E_{OUT}\\
        \Delta U &= \left( W_{comp} + H_{1} \right) - (Q_{ac}) \\
        Q_{ac}   &= W_{comp} + H_{1} - \Delta U \\
    \end{aligned}
\end{equation} 

Its mandatory to solve for the enthalpy ($H_{1}$) entering the air tank, but first the specific heat of ambient air must be determined using polynomial Equation~\ref{eq:Cp_T1} and the Table~\ref{table:Cp_air} of specific heat constants for air.

\begin{table}[ht]
    \newcolumntype{s}{>{\centering\arraybackslash}>{}X}
    \caption{Specific Heat Constants of Air from Table A-2}%
    \arraybackslash%
    \begin{tabularx}{\textwidth}{X c}%
    \textbf{Constant} & \textbf{$\frac{kJ}{kmol\cdot K}$} \\%
    \hline  \\
    a & 28.11 \\
    b & 0.1967 $\cdot 10^{-2} K^{-1}$ \\
    c & 0.4802 $\cdot 10^{-5} K^{-2}$ \\
    d & -1.966 $\cdot 10^{-9} K^{-3}$ \\
    \end{tabularx}    
    \label{table:Cp_air} 
\end{table}

\begin{equation} \label{eq:Cp_T1}
\begin{aligned}
        Cp_{@T_{1}} &= \frac{a+bT_{1}+cT_{1}^2+dT_{1}^3}{M_{AIR}} \\
%                    &= (28.11) + (0.1967 \cdot 10^{-2})(293.15) + \\ &(0.4802 \cdot 10^{-5})(293.15)^2+(-1.966 \cdot 10^{-9})(293.15)^3 \\   
%        Cp_{@T_{1}} &= \frac{Cp_{@T_{1}}}{M_{AIR}} \\
                    &= 1.00275343 \text{ $\frac{kJ}{kmol\cdot K}$}\\
\end{aligned}
\end{equation} 

Further, need to solve for mass of the empty air tank ($m_{EMPTY}$).

\begin{equation} \label{eq:m_empty}
    \begin{aligned}
        m_{EMPTY} &= \frac{P_{1} V_{1}}{R T_{1}}\\
%                  &= \frac{(100 kPa) (0.5 m^3)}{(0.2870 \frac{kJ}{kmol\cdot K} (293.15 K)}\\
                  &= 0.594289708 \text{ kg} \\
    \end{aligned}
\end{equation} 

\begin{equation} \label{eq:H_1}
    \begin{aligned}
        H_{1} &= Cp_{@T_{1}} T_{1} \Delta m_{IN}\\
              &= Cp_{@T_{1}} T_{1}(m_{FULL} - m_{EMPTY}) \\
%              &= (1.00275343 \text{ $\frac{kJ}{kmol\cdot K}$}) (293.15 \text{ K}) (75.39454815 \text{ kg} - 0.594289708 \text{ kg}) \\
              &= 21988.07215 \text{ kJ} \\
    \end{aligned}
\end{equation}

Solve for initial internal energy of the empty tank, $U_{1}$, using $Cv_{@T_{1}}$ (Equation~\ref{eq:Cv_T1} and ~\ref{eq:U1}).

\begin{equation} \label{eq:Cv_T1}
    \begin{aligned}
        Cv_{@T_{1}} &= Cp_{@T_{1}} - R\\
%                    &= 1.00275343 \text{ $\frac{kJ}{kmol\cdot K}$} - 0.2870 \text{ $\frac{kJ}{kmol\cdot K}$}\\
                    &= 0.71575343 \text{ $\frac{kJ}{kmol\cdot K}$}\\
    \end{aligned}
\end{equation} 

\begin{equation} \label{eq:U1}
    \begin{aligned}
        U_{1} &= Cv_{@T_{1}} T_{1} m_{EMPTY}\\
%              &= (0.71575343 \text{ $\frac{kJ}{kmol\cdot K}$}) (293.15 \text{ K}) (0.594289708 \text{ kg}) \\
              &= 124.6957196 \text{ kJ} \\
    \end{aligned}
\end{equation}

Solving for $Cv_{@T_{3}}$ to calculate final internal energy of the full tank, $U_{3}$. 

\begin{equation} \label{eq:Cp_T3}
\begin{aligned}
        Cp_{@T_{3}} &= \frac{a+bT_{3}+cT_{3}^2+dT_{3}^3}{M_{AIR}} \\
%                    &= (28.11) + (0.1967 \cdot 10^{-2})(693.2173432) \\ & + (0.4802 \cdot 10^{-5})(693.2173432)^2+(-1.966 \cdot 10^{-9})(693.2173432)^3 \\   
%        Cp_{@T_{3}} &= \frac{Cp_{@T_{3}}}{M_{AIR}} \\
                    &= 1.074429951 \text{ $\frac{kJ}{kmol\cdot K}$}\\
\end{aligned}
\end{equation} 

\begin{equation} \label{eq:Cv_T3}
    \begin{aligned}
        Cv_{@T_{3}} &= Cp_{@T_{3}} - R\\
%                    &= 1.074429951 \text{ $\frac{kJ}{kmol\cdot K}$} - 0.2870 \text{ $\frac{kJ}{kmol\cdot K}$}\\
                    &= 0.787429951 \text{ $\frac{kJ}{kmol\cdot K}$}\\
    \end{aligned}
\end{equation} 

\begin{equation} \label{eq:U3}
    \begin{aligned}
        U_{3} &= Cv_{@T_{3}} T_{3} m_{FULL}\\
%              &= (0.787429951 \text{ $\frac{kJ}{kmol\cdot K}$}) (693.2173432 \text{ K}) (75.39454815 \text{ kg}) \\
              &= 41154.87548 \text{ kJ} \\
    \end{aligned}
\end{equation}

Finally solving Equation~\ref{eq:Q_ac}. 

\begin{equation} \label{eq:Q_ac_2}
    \begin{aligned}
        Q_{ac}   &= W_{comp} + H_{1} - \Delta U \\
                 &= W_{comp} + H_{1} - (U_{3} - U_{1}) \\
%                 &= 75025.17807 \text{ kJ} + 21988.07215 \text{ kJ} - 41154.87548 \text{ kJ} + 124.6957196 \text{ kJ} \\
                 &= 55983.07046 \text{ kJ} \\
    \end{aligned}
\end{equation} 

To determine the work produced by the turbine ($W_{turb}$), the driving control volume in Figure~\ref{fig:control_vol_driving}.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{images/driving_control_vol.png}
    \caption{Control volume of the driving processes}
    \label{fig:control_vol_driving}
\end{figure}

Equation~\ref{eq:Wturb} is obtained from the driving control volume. 

\begin{equation} \label{eq:Wturb}
    \begin{aligned}
        \Delta U &= E_{IN} - E_{OUT}\\
        \Delta U &= (0) - (W_{turb} + H_{5}) \\
        W_{turb} &= -\Delta U - H_{5} \\
                 &= -(U_{5} - U_{3}) - H_{5} \\ 
           U_{5} &= U_{1} \\
   U_{5} - U_{3} &=  -(U_{3} - U_{1})\\
        W_{turb} &= (U_{3} - U_{1}) - H_{5} \text{, } H_{5} = H_{1} \\
                 &= (U_{3} - U_{1}) - H_{1} \\
%                 &= (41154.87548\text{ kJ}) - (124.6957196\text{ kJ}) - (21988.07215 \text{ kJ}) \\
                 &= 19042.10761 \text{ kJ} \\
    \end{aligned}
\end{equation} 

The mass ratio of the AirCar is determined in Equation~\ref{eq:mass_ratio}.

\begin{equation} \label{eq:mass_ratio}
    \begin{aligned}
        m_{ratio} &= \frac{m_{FULL}}{m_{EMPTY}} \\
%                 &= \frac{75.39454815\text{ kg}}{0.594289708 \text{ kg}}\\
                  &= 126.8649737 \\
    \end{aligned}
\end{equation} 

The efficiency is then calculated below.

\begin{equation} \label{eq:efficiency}
    \begin{aligned}
        \eta &= \frac{benefit}{cost} = \frac{W_{turb}}{W_{comp}}\\
%             &= \frac{19042.10761 \text{ kJ}}{75025.17807 \text{ kJ}}\\
             &= 25.38095624 \% \\
    \end{aligned}
\end{equation} 

\subsubsection{Mechanics Analysis}\label{mech}
The calculation for mechanics of the AirCar are done based on the initial conditions shown in Table~\ref{table:initial_conditions_mech}.

\begin{table}[ht]
    \newcolumntype{s}{>{\centering\arraybackslash}>{}X}
    \caption{Initial Conditions}%
    \arraybackslash%
    \begin{tabularx}{\textwidth}{X X c}%
    \textbf{Properties} & \textbf{} & \textbf{} \\%
    \hline  \\
    Maximum velocity &($V_{f}$) &  70 $\frac{km}{hr}$\\
    Initial velocity &($V_{i}$) & 0 $\frac{km}{hr}$\\
    Time to reach $V_{max}$ &($t$) & 12 s \\
    Coefficient of friction &($f$) & 0.3  \\
    Mass of the car &($m_{CAR}$) & 180 kg \\
    Mass of the driver &($m_{DRVIVER}$) & 75 kg \\
    Mass of the passenger &($m_{PASSENGER}$) & 75 kg \\  
    \end{tabularx}    
    \label{table:initial_conditions_mech} 
\end{table}

Calculating the distance it take for the AirCar to accelerate, $\Delta X_{acc}$.

\begin{equation} \label{eq:x_acc}
    \begin{aligned}
        \Delta X_{acc} &= \frac{V_{i}+V_{f}}{2}t \\
%             &= \frac{(0 \text{ $\frac{m}{s}$})+(70)(\frac{1000 \text{ hr m}}{3600 \text{ s km}})}{2}(12 \text{s})\\
             &= 116.6666667 \text{ m}\\
    \end{aligned}
\end{equation} 

The acceleration of the vehicle is determined by Equation~\ref{eq:accel} below.

\begin{equation} \label{eq:accel}
    \begin{aligned}
        a &= \frac{\Delta V}{\Delta t} \\
%          &= \frac{(70)\frac{1000 \text{ hr m}}{3600 \text{ s km}}}{12 \text{s}}\\
          &= 1.62037037 \text{ $\frac{m}{s^{2}}$}\\
    \end{aligned}
\end{equation} 

To determine the work done to accelerate the vehicle, the force balance in Figure~\ref{fig:force_balance} is used to formulate Equation~\ref{eq:force_bal}.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.8]{images/force_bal.png}
    \caption{Control volume of the driving processes}
    \label{fig:force_balance}
\end{figure}

\begin{equation} \label{eq:force_bal}
    \begin{aligned}
        W_{acc} &= (F + F_{f})\Delta X_{acc} \\
    \end{aligned}
\end{equation} 

Solving for the total mass of the car, $m_{total-full}$.

\begin{equation} \label{eq:m_total}
    \begin{aligned}
        m_{total-full} &= m_{CAR} + m_{DRIVER} + m_{TANK} \text{,  } m_{TANK} = m_{FULL} \\
                  &= 180 \text{ kg} + 75 \text{ kg} + 75.39454815 \text{ kg} \\ 
                  &= 330.3945482 \text{ kg} \\
    \end{aligned}
\end{equation} 

Calculating \emph{F} and \emph{$F_{f}$}.

\begin{equation} \label{eq:forces}
    \begin{aligned}
        F &= m_{total-full} \cdot a \\
%          &= (330.3945482 \text{ kg}) (1.62037037 \text{ $\frac{m}{s^{2}}$}) \\
          &= 535.3615364 \text{ N} \\
    F_{f} &= f\cdot F_{N} \\
          &= f(m_{total-full}g) \\
%          &= (0.3)(330.3945482 \text{ kg})(9.81 \text{ $\frac{m}{s^{2}}$}) \\
          &= 972.3511552 \text{ N} \\
    \end{aligned}
\end{equation} 

Thus using the calculated forces, solving the work done accelerate the AirCar.

\begin{equation} \label{eq:w_acc}
    \begin{aligned}
        W_{acc} &= (F + F_{f})\Delta X_{acc} \\
%                &= (535.3615364 \text{ N} + 972.3511552 \text{ N})(116.6666667 \text{ m}) \\
                &= 175.899814 \text{ kJ} \\
    \end{aligned}
\end{equation} 

Next, determining the mass of the tank after the acceleration stage, $m_{post-accel}$.

\begin{equation} \label{eq:m_post_accel}
    \begin{aligned}
        m_{post-accel} &= m_{total-full} - \frac{W_{acc}}{W_{turb}}(m_{FULL} - m_{EMPTY}) \\
%                  &=  330.3945482 \text{kg} - \frac{175.899814 \text{kJ}}{19042.10761 \text{kJ}}(75.39454815 \text{kg} - 0.594289708 \text{kg}) \\ 
                  &= 329.7035873 \text{ kg} \\
    \end{aligned}
\end{equation} 

In addition, determining the mass of the vehicle without any air in the tank, $m_{total-empty}$.
 
\begin{equation} \label{eq:m_total-empty}
    \begin{aligned}
        m_{total-empty} &= m_{CAR} + m_{DRIVER} + m_{EMPTY} \\
                  &= 180 \text{ kg} + 75 \text{ kg} + 0.594289708 \text{ kg} \\ 
                  &= 255.5942897 \text{ kg} \\
    \end{aligned}
\end{equation} 

Determine the average mass, $m_{avg-cv}$ after the acceleration stage to be used to calculate the distance travelled at constant velocity, $\Delta X_{cv}$.

\begin{equation} \label{eq:m_average-cv}
    \begin{aligned}
        m_{avg-cv} &= \frac{m_{post-accel} + m_{EMPTY}}{2}\\
                   &= \frac{329.7035873 \text{ kg} + 0.594289708 \text{ kg}}{2}\\ 
                   &= 292.6489385 \text{ kg} \\
    \end{aligned}
\end{equation} 

Determining $\Delta X_{cv}$ by solving Equation~\ref{eq:x_cv}.

\begin{equation} \label{eq:x_cv}
    \begin{aligned}
        W_{turb} - W_{acc} &= F_{f}\Delta X_{cv} \\
        \Delta X_{cv} &= \frac{W_{turb} - W_{acc}}{F_{f}} \\
                      &= \frac{ W_{turb} - W_{acc} }{fm_{ave-cv}g} \\
%                      &= \frac{ 19042.10761\text{kJ} - 175.899814\text{kJ}}{(0.3)(292.6489385 \text{ kg})(9.81 \text{ $\frac{m}{s^{2}}$})} \\
             &= 21.90520886 \text{ km}\\
    \end{aligned}
\end{equation} 

Finally calculating the maximum distance of the vehicle, $X_{max}$ neglecting the acceleration. Therefore the $m_{ave-max} = \frac{m_{total-full}+m_{total-empty}}{2}$. 

\begin{equation} \label{eq:x_max}
    \begin{aligned}
        \Delta X_{max} &= \frac{W_{turb}}{F_{f}} \\
                      &= \frac{ W_{turb} }{fm_{ave-max}g} \\
%                      &= \frac{ 19042.10761\text{kJ}}{(0.3)(292.994419\text{ kg})(9.81 \text{ $\frac{m}{s^{2}}$})} \\
             &= 22.08337286 \text{ km}\\
    \end{aligned}
\end{equation} 

Using the same approach the distances of travel are calculated for the driver with one passenger. The results are summarized in Table~\ref{table:distances}.

\begin{table}[ht]
    \newcolumntype{s}{>{\centering\arraybackslash}>{}X}
    \caption{AirCar range at initial conditions from Table~\ref{table:initial_conditions}}%
    \arraybackslash%
    \begin{tabularx}{\textwidth}{X c c}%
    \textbf{Distance} & \textbf{Driver only (km)} & \textbf{Driver and one passenger (km)} \\%
    \hline  \\
    $X_{acc}$ &   0.11666667  &  0.11666667 \\
    $X_{cv}$  &  21.90520886  & 17.39966537 \\
    $X_{max}$ &  22.08337286  & 17.5826172  \\    
    \end{tabularx}    
    \label{table:distances} 
\end{table}

\subsection{Variable Ambient Temperature}

Figure~\ref{fig:variable_temp} on page~\pageref{fig:variable_temp} illustrates the effects of varying ambient temperature has on the AirCar's performance. The efficiency of the vehicle decreases as ambient temperature increases because there is less heat transfer from the air cooler (less energy is removed from the air) thereby decreasing the energy output to the turbines. Therefore the decreasing the range of the car by approximately 9\% or 2km over the -40\degree C to 20 \degree C temperature range for both the driver only and driver and passenger scenario.  The results are summarized in Table~\ref{table:summary_temp} on page~\pageref{table:summary_temp}.

\subsection{Variable Pressure Ratio}

Figure~\ref{fig:variable_pressure} on page~\pageref{fig:variable_pressure} shows the effects of varying the pressure ratio on the performance of the AirCar. As the pressure ratio increases the efficiency of the vehicle increases, because more mass can be stored in the tank thus more energy that can be converted to be used for the turbine. Low pressures prevent air storage in the tank thereby minimizing the range of the car. Varying pressure ratio has a dramatic effect on the car's performance which is summarized in Table~\ref{table:summary_pressure} on page~\pageref{table:summary_pressure}. Varying the pressure ratio from 0 - 300 causes the kilometre range to vary from 0 - max km, respectively.

\subsection{Variable Effectiveness of the Air Cooler}

Figure~\ref{fig:variable_effectiveness} on page~\pageref{fig:variable_effectiveness} describes the effects of varying effectiveness of the air cooler has on the AirCar's performance. The amount of air that can be compressed into the air tank increases non linearly with effectiveness of the cooler. The variation of effectiveness has a big impact on the vehicle's efficiency, as the effectiveness increases the efficiency drops in a linear manner. As the temperature of air in the tank ($T_{3}$) approaches the ambient temperature ($T_{1}$), the amount of work done by the cooler ($Q_{ac}$) approaches the amount of work done to compress the air into the tank ($W_{comp}$). Therefore the energy available for the turbines decreases. 

When the effectiveness surpasses 0.9\% $Q_{ac}$ becomes greater than the work done by the compressor, thus $W_{turb}$ becomes negative and the surrounding is doing work on the turbines. The results are summarized in Table~\ref{table:summary_effectiveness} on page~\pageref{table:summary_effectiveness}.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.9]{images/temp_vs_energy.pdf}
    \includegraphics[scale=0.9]{images/temp_vs_efficiency.pdf}
    \includegraphics[scale=0.9]{images/temp_vs_distance.pdf}
    \caption{Effects of Varying Ambient Temperature}
    \label{fig:variable_temp}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.9]{images/pressure_vs_energy.pdf}
    \includegraphics[scale=0.9]{images/pressure_vs_efficiency.pdf}
    \includegraphics[scale=0.9]{images/pressure_vs_distance.pdf}
    \caption{Effects of Varying Pressure Ratio $P_{2}/P_{1}$}
    \label{fig:variable_pressure}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.9]{images/effectiveness_vs_energy.pdf}
    \includegraphics[scale=0.9]{images/effectiveness_vs_efficiency.pdf}
    \includegraphics[scale=0.9]{images/effectiveness_vs_mass.pdf}
    \includegraphics[scale=0.9]{images/effectiveness_vs_temp.pdf}
    \caption{Effects of Varying Pressure Ratio $P_{2}/P_{1}$}
    \label{fig:variable_effectiveness}
\end{figure}

\begin{table}[ht]
\newcolumntype{s}{>{\centering\arraybackslash}>{}X}
    \caption{Effects of Varying Ambient Temperature}%
    \arraybackslash%
    \centerline{\begin{tabularx}{1.14\textwidth}{c c c c c c c}%
    \textbf{$T_{1}$($\degree C$)} & \textbf{$W_{comp}$(kJ)} & \textbf{$Q_{ac}$(kJ)} & \textbf{$W_{turb}$(kJ)} & \textbf{$\eta$ (\%)} & \textbf{Driver(km)} & \textbf{Passenger(km)} \\%
    \hline \\
    -20 & 75025.178 & 56830.037 & 18195.141 & 24.252 & 20.677 & 16.530\\    -10 & 75025.178 & 56620.705 & 18404.473 & 24.531 & 21.033 & 16.796\\      0 & 75025.178 & 56409.585 & 18615.593 & 24.812 & 21.386 & 17.060\\      10 & 75025.178 & 56196.949 & 18828.229 & 25.096 & 21.736 & 17.322\\     20 & 75025.178 & 55983.070 & 19042.108 & 25.381 & 22.083 & 17.583\\      30 & 75025.178 & 55768.221 & 19256.957 & 25.667 & 22.428 & 17.842\\     40 & 75025.178 & 55552.674 & 19472.504 & 25.955 & 22.771 & 18.099\\
    \end{tabularx}}
    \label{table:summary_temp}
\end{table}

\begin{table}[ht]
\newcolumntype{s}{>{\centering\arraybackslash}>{}X}
    \caption{Effects of Varying Pressure Ratio $P_{2}/P_{1}$}%
    \arraybackslash%
    \begin{tabularx}{1.1\textwidth}{c c c c c c c}%
    \textbf{$r_{p}$} & \textbf{$W_{comp}$(kJ)} & \textbf{$Q_{ac}$(kJ)} & \textbf{$W_{turb}$(kJ)} & \textbf{$\eta$ (\%)} & \textbf{Driver(km)} & \textbf{Passenger(km)} \\%
    \hline \\
    50   &  9165.838 &  7607.538 &  1558.300 & 17.001 &  2.007 &  1.562\\
    100  & 21078.391 & 16699.714 &  4378.677 & 20.773 &  5.499 &  4.306\\
    150  & 33885.779 & 26218.773 &  7667.006 & 22.626 &  9.417 &  7.409\\
    200  & 47239.634 & 35985.556 & 11254.078 & 23.823 & 13.544 & 10.702\\
    250  & 60982.570 & 45921.503 & 15061.067 & 24.697 & 17.784 & 14.107\\
    300  & 75025.178 & 55983.070 & 19042.108 & 25.381 & 22.083 & 17.583\\    \end{tabularx}
    \label{table:summary_pressure}
\end{table}

\begin{table}[ht]
\newcolumntype{s}{>{\centering\arraybackslash}>{}X}
    \caption{Effects of Varying Effectiveness of the Air Cooler}%
    \arraybackslash%
\begin{tabularx}{\textwidth}{X c c c c c X}%
    \textbf{$\epsilon$} & \textbf{$T_{3}$(K)} & \textbf{$m_{FULL}$} & \textbf{$W_{comp}$(kJ)} & \textbf{$Q_{ac}$(kJ)} & \textbf{$W_{turb}$(kJ)} & \textbf{$\eta$ (\%)}\\%
    \hline \\
0.0  & 1111.932 &  47.004 &  47571.100 &  16089.973 &  31481.127 & 66.177\\
0.25 &  908.486 &  57.530 &  58224.120 &  31958.558 &  26265.563 & 45.111\\
0.5  &  705.041 &  74.130 &  75025.178 &  55875.750 &  19149.428 & 25.524\\
0.75 &  501.595 & 104.197 & 105455.143 &  97354.735 &   8100.408 &  7.681\\
1.0  &  298.150 & 175.297 & 177413.456 & 192363.456 & -14950.000 & -8.427\\\end{tabularx}
    \label{table:summary_effectiveness}
\end{table}
 \clearpage

\subsection{Discussion and Recommendations}
Ambient temperature has little effect on the AirCar's efficiency and range. Therefore the vehicle can operate in temperature ranges of -20 to 40\degree C and have a reasonable performance, although decreasing by ~10\% at the temperature reaches 40\degree C. 

The pressure ratio between ambient pressure and the pressure of air stored in the tank should be maximized for maximum range and efficiency of the vehicle. The current existing technology allows for pressure ratios of 300, therefore the design is sound with regards to pressure.

The effectiveness of the air cooler has the greatest effect on the efficiency of the vehicle. Although the amount of air mass stored (energy) in the tank increases with air cooler effectiveness, the energy output by the turbines decreases dramatically. From results, its deduced that the efficiency is at its maximum when the temperature of the air inside the tank is approximately $T_{2}$, thus the effectiveness of the cooler is zero. However, realistically that is impossible to design a thermally insulated tank that must withhold 30MPa of air at the temperature of 830\degree C. 

From the ideal case described throughout the report, the AirCar design is approximately 20\% efficient, which is 10\% than diesel engines and about the same at petrol engines, with dramatically less range. Although the vehicle is environmentally friendly in term of its operation, it will not replace gas driver automobiles.

The AirCar is recommended for small range travels and without load. An example application could be a warehouse supervisor, that must get from A to B in a quick manner.
\backmatter

%\bibliography{uw-wkrpt-bib}
\appendix
    
\end{document}