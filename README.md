**University of Waterloo LaTEX Reports
**

MTE 202 - Ordinary Differential Equations

- Second Order ODEs

MTE 203 - Advanced Calculus 

- Extrema of Functions
- Multiple Integration

ME 321 - Kinematics and Dynamics of Machines

- Scaled Segway Stub Axle Safety Verification

- Gear and Shaft Design

- Taipei 101 Tuned Mass Damper Analysis

MTE 325 - Microprocessor Systems and Interfacing 

- Interfacing with Interrupts and Synchronization Techniques

MTE 309 - Introduction to Thermodynamics and Heat Transfer

- Compressed Air Car

Work Term Reports 

- Nymi Inc - Unit Test Harness Design and Adaptation