% initialize matlab environment
close all;
clear all;
clc;
format short;

%% PART 1
% PART 1A
% creating the mesh for the plot with domain specified at 1 <= x <= 5.5 and -4 <= y <= 3 with fixed increment of 0.1 for deltaX and deltaY
[xnum,ynum] = meshgrid(1 : 0.1 : 5.5 , -4 : 0.1 : 3);

% surface equation in form of znum = f(xnum,ynum)
znum = @(xnum, ynum) 0.00125.*exp(-((xnum-3).^2+(0.5).*ynum.^2)).*(sin(2.*xnum)+2.*sin(0.75.*(0.5*ynum-2).^2)).*(16.*xnum+64.*xnum.^2+ynum.^2);

% ploting the terrain as a 3D surface
figure
surfc(xnum,ynum,znum(xnum,ynum));
xlabel('Distance in the East direction [km]');
ylabel('Distance in the North direction [km]');
zlabel('Elevation above sea level [km]');
c = colorbar;
c.Label.String = 'Elevation above sea level [km]';
title('3D surface of the Terrain');

% ploting the contour map of the terrain over 30 levels
figure
[contour1, h]= contour(xnum,ynum,znum(xnum,ynum), 30);
xlabel('Distance in the East direction [km]');
ylabel('Distance in the North direction [km]');
zlabel('Height of the Terrain above sea level [km]');
c = colorbar;
c.Label.String = 'Elevation above sea level [km]';
%clabel(contour1, h);
title('Contour map of the Terrain');

% definition
syms x y z z_ l;
%surface equation in form of z = f(x,y)
z = symfun(0.00125.*exp(-((x-3)^2+(0.5).*y^2)).*(sin(2.*x)+2.*sin(0.75.*(0.5*y-2)^2)).*(16.*x+64.*x^2+y^2), [x y]);

% PART 1B
% taking first derivatives of the terrain function
zx(x,y) = diff(z,x);
zy(x,y) = diff(z,y);

% solving for gradient at every point in the bounded terrain 
maxGrad = (gradient(z, [x,y]));
maxGrad_mag = 0;
for i = 1 : 0.1 : 5.5
    for j = -4 : 0.1 : 3
        new_maxGrad_mag = maxGrad(i,j);
        new_maxGrad_mag = sqrt(new_maxGrad_mag(1)^2 + new_maxGrad_mag(2)^2);
        if(new_maxGrad_mag > maxGrad_mag)
            maxGrad_mag = double(new_maxGrad_mag);
            indexMax = [i j];
        end
    end
end
disp(maxGrad_mag)
disp(indexMax)

% PART 1C
% define function handler 
f = matlabFunction([zx zy]);
f = @(t) f(t(1), t(2));

% make initial guesses from the plotted graph
ip1 = [2.9, -0.8];
ip2 = [3.5, -2.0];
ip3 = [3.6,  1.1];

% FIRST DERIVATIVE TEST : solve for the critical points
cp1(1,:) = fsolve(f, ip1);
cp2(1,:) = fsolve(f, ip2);
cp3(1,:) = fsolve(f, ip3);

% displaying values of the terrain function at the Critical Points
zCP1 = double(z(cp1(1,1), cp1(1,2)));
zCP2 = double(z(cp2(1,1), cp2(1,2)));
zCP3 = double(z(cp3(1,1), cp3(1,2)));

% SECOND DERIVATIVE TEST : taking second derivatives of the terrain function
A(x,y) = diff(zx,x); % zxx(x,y)
C(x,y) = diff(zy,y); % zyy(x,y)
B(x,y) = diff(zx,y); % zxy(x,y)

% determine A B C for each point for the table in the report
double(A(cp1(1,1),cp1(1,2)));
double(B(cp1(1,1),cp1(1,2)));
double(C(cp1(1,1),cp1(1,2)));

double(A(cp2(1,1),cp2(1,2)));
double(B(cp2(1,1),cp2(1,2)));
double(C(cp2(1,1),cp2(1,2)));

double(A(cp3(1,1),cp3(1,2)));
double(B(cp3(1,1),cp3(1,2)));
double(C(cp3(1,1),cp3(1,2)));

% classify each Critical Point by finding the Hessian of the terrain z(x,y)
% D = B^2 - AC
D(x,y) = (B(x,y).^2) - A(x,y) * C(x,y);

% plugging in the CP points into the D equation
D1 = double(D(cp1(1,1),cp1(1,2)));
D2 = double(D(cp2(1,1),cp2(1,2)));
D3 = double(D(cp3(1,1),cp3(1,2)));

% classify the points based on D and A
classCritPoint(D1, A(cp1(1,1),cp1(1,2)), cp1);
classCritPoint(D2, A(cp2(1,1),cp1(1,2)), cp2);
classCritPoint(D3, A(cp3(1,1),cp1(1,2)), cp3);

%% PART 2

% Temperature distribution within the limits of the terrain is a function
% of the height and it is given by T(x,y,z) in [deg C], and x,y,z in km
Tnum = @(xnum, ynum) -0.1*(znum(xnum,ynum).^2) + 17*exp(-0.1*((0.1*xnum-2)-(0.05*ynum-1).^2-(znum(xnum,ynum)-1).^2))-10;
Tnum_3vars = @(xnum, ynum, znum) -0.1*(znum.^2) + 17*exp(-0.1*((0.1*xnum-2)-(0.05*ynum-1).^2-(znum-1).^2))-10;

T = symfun(-0.1*(z(x,y)^2) + 17*exp(-0.1*((0.1*x-2)-(0.05*y-1)^2-(z(x,y)-1)^2))-10, [x y]);
T_3vars = symfun(-0.1*(z_.^2) + 17*exp(-0.1*((0.1*x-2)-(0.05*y-1).^2-(z_-1).^2))-10, [x y z_]);

% PART 2A
% Calculating temperatures at highest and lowest elevations on the terrain
tLowElevation = double(T(cp1(1,1), cp1(1,2)))
tHighElevation = double(T(cp3(1,1), cp3(1,2)))

% PART 2B
% PART 2Bi
% Calculating temperature at point (4,-0.3)
P = [4 -0.3];
tempPoint = double(T(P(1,1), P(1,2)));

% PART 2Bii
height = znum(P(1,1), P(1,2))

figure;
contour(xnum, ynum, Tnum_3vars(xnum, ynum, height), 30);
xlabel('Distance in the East direction [km]');
ylabel('Distance in the North direction [km]');
zlabel('Height of the Terrain above sea level [km]');
c = colorbar;
c.Label.String = 'Temperature based on elevation above sea level [deg C]';
title('Isotherms at point(4, -0.3)');

%PART 2C 
%PART 2Ci
u = [-1 1];
u = u / sqrt(u(1)^2 + u(2)^2);

% calculating gradient 
grad(x,y) = [zx, zy];

% calculating the directional gradient at point P(4,-0.3)
dir_grad = double(dot(grad(P(1),P(2)),u))
if (dir_grad >0)
    fprintf('Hiker traveling in Northwest direction is ascending');
else
    fprintf('Hiker traveling in Northwest direction is descending');
end

% PART 2Cii
Tx(x,y,z_) = diff(T,x);
Ty(x,y,z_) = diff(T,y);
Tz(x,y,z_) = diff(T,z_);

gradT(x, y, z_) = [Tx, Ty, Tz];

changeZ = -1 * zx(P(1),P(2)) + 1 * zy(P(1),P(2));
u2 = [-1 1 changeZ]
u2 = u2 / sqrt(u2(1)^2 + u2(2)^2 +u2(3)^2)

% calculating the directional gradient
dir_grad2 = double(dot(gradT(P(1),P(2), changeZ),u2))

%PART 2D 
%PART 2Di
u3 = [-1 -1];
u3 = u3 / sqrt(u3(1)^2 + u3(2)^2);

% calculating the directional gradient at point P(4,-0.3)
dir_grad3 = double(dot(grad(P(1),P(2)),u3))
if (dir_grad3 >0)
    fprintf('Hiker traveling in SouthWest direction is ascending');
else
    fprintf('Hiker traveling in SouthWest direction is descending');
end

% PART 2Dii
changeZ4 = -1 * zx(P(1),P(2)) + -1 * zy(P(1),P(2));
u4 = [-1 -1 changeZ4]
u4 = u4 / sqrt(u4(1)^2 + u4(2)^2 +u4(3)^2)

% calculating the directional gradient
dir_grad4 = double(dot(gradT(P(1),P(2), changeZ4),u4))

% PART 2E
% ploting the temperature as color of the surface 
figure
surfc(xnum, ynum, znum(xnum,ynum), Tnum(xnum, ynum));
xlabel('Distance in the East direction [km]');
ylabel('Distance in the North direction [km]');
zlabel('Elevation above sea level [km]');
c4 = colorbar;
c4.Label.String = 'Temperature based on elevation above sea level [deg C]';
title('3D surface of the Terrain with Temperature as the color map');

% PART 2F
% ploting the temperature of the terrain as a 3D surface
figure
surfc(xnum,ynum,Tnum(xnum, ynum));
xlabel('Distance in the East direction [km]');
ylabel('Distance in the North direction [km]');
zlabel('Temperature [deg C]');
c = colorbar;
c.Label.String = 'Temperature [deg C]';
title('Temperature of the Terrain');

% PART 2G
L = T_3vars(x, y, z_) + l*(z(x, y) - z_)
Grad_L = gradient(L, [x y z_ l])
T_max_var = vpasolve(Grad_L == 0, [x, y, z_, l], [cp1(1) cp1(2) z(cp1(1), cp1(2)) -20])
T_max = T_3vars(T_max_var.x, T_max_var.y, T_max_var.z_)
