function [ output_args ] = classCritPoint( D, A, cp)
%classPoint Summary of this function goes here
%   Classifying critical points using SECOND DERIVATIVE TEST
%   D < 0 and A < 0 then (x,y) is a REL MAX
%   D < 0 and A > 0 then (x,y) is a REL MIN
%   D > 0 then (x,y) is a SADDLE POINT
%   D == 0 then NON CONCLUSIVE
    
    if (D < 0)
        if (A < 0)
            fprintf('(%f, %f) is a REALTIVE MAXIMUM.\n',cp(1,1),cp(1,2));
        else
            fprintf('(%f, %f) is a REALTIVE MINIMUM.\n',cp(1,1),cp(1,2));
        end
    elseif (D > 0)
        fprintf('(%f, %f) is a SADDLE POINT.\n',cp(1,1),cp(1,2));
    else
        fprintf('(%f, %f) is UNCONCLUSIVE.\n',cp(1,1),cp(1,2));
    end
end

