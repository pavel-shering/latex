\select@language {english}
\contentsline {section}{Summary}{v}{section*.3}
\contentsline {section}{\numberline {1} \phantom {}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1} \phantom {}Background of the Nymi Band}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2} \phantom {}Principles of \acrlong {TDD}}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3} \phantom {}Importance of \acrlong {TDD}}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4} \phantom {}Objective and Method}{4}{subsection.1.4}
\contentsline {section}{\numberline {2} \phantom {}Analysis of Unit Testing Framework}{5}{section.2}
\contentsline {subsection}{\numberline {2.1} \phantom {}Drawback 1: Structure}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2} \phantom {}Drawback 2: Build Target}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3} \phantom {}Drawback 3: Uni-platform build}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4} \phantom {}Summary of General Unit Test Framework}{7}{subsection.2.4}
\contentsline {section}{\numberline {3} \phantom {}Design Constraints}{8}{section.3}
\contentsline {subsection}{\numberline {3.1} \phantom {}Description of Design Constraints}{8}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1} \phantom {}Simplicity}{8}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2} \phantom {}Portability}{8}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3} \phantom {}Product Cost}{9}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4} \phantom {}Expandability}{9}{subsubsection.3.1.4}
\contentsline {subsection}{\numberline {3.2} \phantom {}Structure}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3} \phantom {}Weighting of Design Constraints}{9}{subsection.3.3}
\contentsline {section}{\numberline {4} \phantom {}Design of Unit Test Harness}{11}{section.4}
\contentsline {subsection}{\numberline {4.1} \phantom {}Unit Framework Selection}{12}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1} \phantom {}Unity}{13}{subsubsection.4.1.1}
\contentsline {subsection}{\numberline {4.2} \phantom {}Unity Test Framework Adaptation}{14}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3} \phantom {}Unit Test Driver Design}{16}{subsection.4.3}
\contentsline {section}{\numberline {5} \phantom {}Design Evaluation}{23}{section.5}
\contentsline {section}{\numberline {6} \phantom {}Conclusion}{25}{section.6}
\contentsline {section}{\numberline {7} \phantom {}Recommendations}{26}{section.7}
\contentsline {section}{\hbox to\@tempdima {8\hfil }{References}}{27}{section*.19}
\contentsline {section}{Glossary}{28}{section*.20}
\contentsline {section}{\numberline {Appendix A} \phantom {Appendix }List of Assertions for Unity}{29}{appendix.A}
\contentsline {section}{\numberline {Appendix B} \phantom {Appendix }Unity Test Output}{33}{appendix.B}
\contentsline {section}{\numberline {Appendix C} \phantom {Appendix }Summary Generation}{34}{appendix.C}
\contentsline {section}{\numberline {Appendix D} \phantom {Appendix }Report Generation}{35}{appendix.D}
