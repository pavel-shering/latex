\contentsline {section}{\numberline {1} \phantom {}Summary of Analysis}{2}
\contentsline {subsection}{\numberline {1.1} \phantom {}Volume of Shell}{3}
\contentsline {subsection}{\numberline {1.2} \phantom {}Volume of Heat Exchanger Tube}{5}
\contentsline {subsection}{\numberline {1.3} \phantom {}Volume of the Cap}{6}
\contentsline {subsection}{\numberline {1.4} \phantom {}Total Volume}{7}
\contentsline {section}{\numberline {2} \phantom {}Summary of Results}{8}
\contentsline {subsection}{\numberline {2.1} \phantom {}Volume of the Shell}{8}
\contentsline {subsection}{\numberline {2.2} \phantom {}Volume of the Tube}{8}
\contentsline {subsection}{\numberline {2.3} \phantom {}Volume of the Cap}{8}
\contentsline {subsection}{\numberline {2.4} \phantom {}Total Volume}{8}
\contentsline {subsection}{\numberline {2.5} \phantom {}Summary of Calculations}{8}
\contentsline {section}{\numberline {Appendix A} \phantom {Appendix }MATLAB Code}{9}
