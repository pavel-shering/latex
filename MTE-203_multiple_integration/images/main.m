close all;
clear all;
clc;
format long;

%% Definition of Problem Variables
% Uncomment different cases to get a plot and the solution for each of the volumes and total volume
% Case 1
L = 1000;
a = 300;
b = 200;
d = 80;

% Case 2
% L = 1600;
% a = 400;
% b = 500;
% d = 60;

%% PART 1A
% creating the mesh for the plot with domain specified with fixed increment for deltaX and deltaY
% u [0,2Pi) v [0,L] where h is the height
[u,v] = meshgrid(0 : pi/30 : 2*pi , -L/2 : 1 : L/2);

% Elliptic Cylinder
x = a*cos(u);
y = v;
z = b*sin(u);

% ploting the Shell as a 3D surface
figure
surf(x,y,z,'FaceColor','red','EdgeColor','none');
hold on;


% Tube Heat Exchanger
[e,f] = meshgrid(0 : pi/30 : 2*pi , -a : 1 : a);

% Cylinder
i = f;          %x
j = d./2*sin(e); %y
k = d./2*cos(e); %z

% ploting the Heat Exchanger tubes as a 3D surface
surf(i,j-d,k,'FaceColor','Yellow','EdgeColor','Yellow');
hold on;
surf(i,j+d,k,'FaceColor','Yellow','EdgeColor','Yellow');
hold on;
surf(i,j-3*d,k,'FaceColor','Yellow','EdgeColor','Yellow');
hold on;
surf(i,j+3*d,k,'FaceColor','Yellow','EdgeColor','Yellow');
hold off;

% set plot properties
title('3D View of the Shell and Tube Heat Exchanger');
xlabel('Width [mm]');
ylabel('Length [mm]');
zlabel('Height [mm]');
axis([-L/2 L/2, -L/2 L/2, -L/2 L/2]);

% set alpha properties to be less than 1 to make surfaces transparent
alpha(0.4);
camlight left;
lighting phong;
legend('Shell', 'Heat Exchanger Tube');
legend('boxoff');

%% PART 1B
% Volume of the Shell
F =@(u,r,y) r;
blim = @(u,r,y) 1. / sqrt(sin(u).^2/b.^2 + cos(u).^2/a.^2);

Vshell = integral3(F, 0, 2*pi, 0, blim, -L/2, L/2);
Vshell = Vshell / 1000.^3

% Volume of the Tube Heat Exchanger
F2 =@(u,r,y) r;

Vtube = integral3(F2, 0, 2*pi, 0, d/2, -a, a);
Vtube = Vtube / 1000.^3

% Volume of the Cap
F3 =@(u,r,x) r;
blim3 = @(u,r,x) a.*sqrt(1-(r.*sin(u)/b).^2);

% In order to solve the integral the method is changed to iterative and the tolerance for convergence are changed accordingly
Vcap = 4* integral3(F2, 0, pi/2, 0, d/2, blim3, a, 'AbsTol',1e-12,'RelTol',1e-9, 'Method', 'iterated');
Vcap = Vcap / 1000.^3

%% PART 1C
% Total Volume in Cubic Meters 
Vtotal = Vshell - 4*Vtube + 8*Vcap
