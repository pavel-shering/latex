% uw-wkrpt-ece.tex - An example work report that uses uw-wkrpt.cls
% Copyright (C) 2002,2003  Simon Law
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% We begin by calling the workreport class which includes all the
% definitions for the macros we will use.
\documentclass[]{uw-wkrpt}

% We will use some packages to add functionality
\usepackage{graphicx} % Include graphic importing
\usepackage{gensymb}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fixltx2e}
\usepackage{array}
\usepackage{longtable}
\usepackage{caption}
\usepackage{tabularx}
\usepackage{float}
\usepackage{pstricks}
\usepackage{listings}
\usepackage{subcaption}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}

\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}


\captionsetup{
    format = plain,
    font = normalsize,
    labelfont = {bf}
}
\graphicspath{ {images/} }
\newcommand{\Mypm}{\mathbin{\tikz [x=1.4ex,y=1.4ex,line width=.1ex] \draw (0.0,0) -- (1.0,0) (0.5,0.08) -- (0.5,0.92) (0.0,0.5) -- (1.0,0.5);}}%
% Now we will begin writing the document.
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% IMPORTANT INFORMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% First we, should create a title page.  This is done below:
% Fill in the title of your report.
\title{Multiple Integration}

% Fill in your name.
\author{Pavel Shering}

% Fill in your student ID number.
\uwid{20523043}

% Fill in your home address.
\address{123 University Ave. W.,\\*
         Waterloo, ON\ \ N2L 3G1}

% Fill in your employer's name.
\employer{Project 2 \\ MTE 203 - Advanced Calculus}

% Fill in your employer's city and province.
\employeraddress{}

% Fill in your school's name.
\school{University of Waterloo}

% Fill in your faculty name.
\faculty{Faculty of Engineering}

% Fill in your e-mail address.
\email{Instructor: Patricia Nieva}

% Fill in your term.
\term{}

% Fill in your program.
\program{}

% Fill in the department chair's name.
\chair{Dr.\ William \ Melek}

% Fill in the department chair's mailing address.
\chairaddress{Mechanical and Mechatronics Engineering Department,\\*
              University of Waterloo,\\*
	      Waterloo, ON\ \ N2L 3G1}

% If you are writing a "Confidential 1" report, uncomment the next line.
%\confidential{Confidential-1}

% If you want to specify the date, fill it in here.  If you comment out
% this line, today's date will be substituted.
\date{December 4, 2015}

% Now, we ask LaTeX to generate the title.
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FRONT MATTER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \frontmatter will make the \section commands ignore their numbering,
%% it will also use roman page numbers.
\frontmatter


% Next, we need to make a Table of Contents, List of Figures and 
% List of Tables.  You will most likely need to run LaTeX twice to
% get these correct.  The first pass for LaTeX to figure out the
% labels, and the second pass to put in the right references.
\tableofcontents
\listoffigures
\listoftables

% We continue with required sections, such as the Contributions and Summary
% \begin{onehalfspacing}
% \clearpage\section{Summary}


% \end{onehalfspacing}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REPORT BODY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \main will make the \section commands numbered again,
%% it will also use arabic page numbers.
\mainmatter

\section*{Abstract}\label{sec:abstract}

The following report demonstrates the use of MATLAB\textsuperscript{\textregistered} mathematical software to solve a real life application of complex integration problems which are impossible to solve symbolically. The objective is to calculate volume of oil required to fill a shell, that is cooled with a tube heat exchanger using numerical triple integration. The resulting total volume of oil required for case 1 and case 2 are: 0.17649 m${^3}$ and 0.99626 m${^3}$, respectively.

\section{Summary of Analysis}\label{sec:analysis}

This section of the report describes the techniques followed to calculate the volume of each of the object in a common oil refinery set up described by Equation \ref{eq:volume} and visualised in Figure \ref{fig:3Dshell}.

\begin{equation} \label{eq:volume}
    \begin{aligned}
        V_{TOTAL} = V_{SHELL} - 4*V_{TUBE} + 8*V_{CAP}
    \end{aligned}
\end{equation} 

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.50]{3DHeatexchanger.png}
    \caption{3D view of the shell and the tube heat exchanger}
    \label{fig:3Dshell}
\end{figure}

Figure \ref{fig:dim} demonstrates the dimensions used in the calculation for total volume required to fill the shell as a function of tube diameter cross section (d), semi axis of the elliptical shell cross section (a and b) and the length of the shell (L).

\begin{figure}[ht]
\begin{subfigure}{0.50\textwidth}
\includegraphics[width=\linewidth]{shellDim.png}
    \caption{\label{fig:front}Front view} %--> Fig.1a
    \vspace{-10 pt}
\end{subfigure}
\hspace*{\fill} % separation between the subfigures
\begin{subfigure}{0.50\textwidth}
    \includegraphics[width=\linewidth]{shellDim2.png}
    \vspace{-10 pt}
    \caption{\label{fig:top}Top view} %--> Fig.1b
\end{subfigure}
\caption{Tube heat exchanger} \label{fig:dim}
\end{figure}

One of problem encountered during the project was to graph the half donut ends of the tube heat exchanger. Through research, it was found how to plot a torus, however it is not clear what parameter varies which dimension of the torus. In addition, graphing the ends actually made the representation of the set up unclear, thus the ends are omitted. Another issue occurred when solving for the volume of the cap in MATLAB where the numerical integration was not solvable by the limits provided. The solution was to change the method of integration to iterative and change the tolerances on convergence.

The following sections describe the process of calculating the respective volumes from Equation \ref{eq:volume} using cylindrical coordinate system for which a general form of triple integral is shown in Equation \ref{eq:cylCoord}.

\begin{equation} \label{eq:cylCoord}
    \begin{aligned}
       V = \int_{\theta = a}^{\theta = b} \int_{r = g_1(\theta)}^{r = g_2(\theta)} \int_{z = f_1(r,\theta)}^{z = f_2(r,\theta)} 1 \,dz\,dr\,d\theta
    \end{aligned}
\end{equation} 

Figure \ref{fig:graph} and \ref{fig:graph2} show simplified graphical representation of the heat exchanger set up for case 1 and 2, respectively, which is simply four cylinders inserted into a elliptical cylinder. The dimensions for case 1 and 2 can be found in a Table \ref{tab:dimensions} in section \ref{sec:calculations} 

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.70]{graph}
    \caption{3D Surface Graph of the Heat Exchanger: Case 1}
    \label{fig:graph}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.70]{graph2}
    \caption{3D Surface Graph of the Heat Exchanger: Case 2}
    \label{fig:graph2}
\end{figure}

\subsection{Volume of Shell} \label{sec:Vshell}
The volume of the Shell is simply the volume of the elliptical cylinder of semi radius \textit{(a)} and \textit{(b)} and length \textit{(L)} as described in Figure \ref{fig:dim}. The elliptical cylinder is described bellow in Equation \ref{eq:ellipse}

\begin{equation} \label{eq:ellipse}
    \begin{aligned}
        \frac{x^2}{a^2} + \frac{z^2}{b^2} = 1
    \end{aligned}
\end{equation} 

Preforming parametrization on Equation \ref{eq:ellipse} to convert into cylindrical coordinate form which is shown in Equation \ref{eq:ellipseParam} 


\begin{equation} \label{eq:ellipseParam}
    \begin{aligned}
        x &= r \cos\theta) \\
        z &= r \sin(\theta) \\
        \therefore \frac{(r\cos(\theta))^2}{a^2} &+ \frac{(r\sin(\theta))^2}{b^2} = 1 \\
    \end{aligned}
\end{equation}

Rearranging Equation \ref{eq:ellipseParam} for \textit{r} to use in the integration limits to achieve Equation \ref{eq:ellipseR}

\begin{equation} \label{eq:ellipseR}
    r =  \frac{1}{\sqrt{\frac{(\cos(\theta))^2}{a^2} + \frac{(\sin(\theta))^2}{b^2}}} 
\end{equation}

When solving symmetric problems, such as an elliptical cylinder revolved about the y-axis, the solution to such integration can be split into a quarter choosing the positive quadrant and multiplying the final answer by four. However, since MATLAB is used for finding the solution the integration can be set up for the entire shape, shown in Equation \ref{eq:ellipseInt}.

\begin{equation} \label{eq:ellipseInt}
       V_{SHELL} = \iiint\displaylimits_V \,dv = \int_{\theta = 0}^{\theta = 2\pi} \int_{r = 0}^{r = {\left(\sqrt{\frac{(\cos(\theta))^2}{a^2} + \frac{(\sin(\theta))^2}{b^2}}\right)}^{-1}} \int_{y = 0}^{y = L} 1 \,dy\,rdr\,d\theta 
\end{equation} 

In Equation \ref{eq:ellipseInt} the integration limits were determined by breaking the problem into three parts. First, taking the length from \textit{0} to \textit{L} seen as the inner most integral limit, along which the radius of the elliptical cross section starts at \textit{0} and goes to the end of the elliptical cylinder equation which was rearranged for the \textit{r} (Equation \ref{eq:ellipseR}). This limit is seen in the second integral of Equation \ref{eq:ellipseInt} which forms a cross sectional area of width \textit{r} stretched along length \textit{L}. Finally, the cross sectional area of varying radius is rotated full 360\degree, or in radians 2${\pi}$ to obtain the volume of the shell.

\subsection{Volume of Heat Exchanger Tube} \label{sec:Vtube}

Calculating the volume of the heat exchange tubes is very similar to the procedure for calculating the volume of the shell (Equation \ref{eq:ellipseInt}), except the tube is a perfect cylinder hence the radius of integration does not vary. Therefore, the cross sectional area of constant width \textit{r} stretched along length \textit{L} is then revolved 2${\pi}$ radians. Equation \ref{eq:tubeInt} describes the volume of the heat exchanger tube. 

\begin{equation} \label{eq:tubeInt}
       V_{TUBE} = \iiint\displaylimits_V \,dv = \int_{\theta = 0}^{\theta = 2\pi} \int_{r = 0}^{r = \frac{d}{2}} \int_{y = -a}^{y = a} 1 \,dy\,rdr\,d\theta 
\end{equation} 

The heat exchanger tube has a length of \textit{2a} which is integrated from \textit{-a} to \textit{a} which is seen in the inner most integral. In addition, the tubes have a circular cross section thus the radius limits are from \textit{0} to \textit{d/2} where \textit{d} is the diameter of the tube.


\subsection{Volume of the Cap} \label{sec:Vcap}
Due to the approach of volume calculation described in Equation \ref{eq:volume}, the cap volume must be added back to the total volume, which is shown in Figure \ref{fig:cap}.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.20]{cap}
    \caption{Cap volume is the volume of the Tube outside Shell}
    \label{fig:cap}
\end{figure}

The cap is the small amount of volume subtracted as part of the total tube from the shell volume. The cap is the volume that is bounded on the \textit{x} axis from \textit{a} to the equation for the ellipse (Equation \ref{eq:ellipse}). However, the cap remains to have a circular cross section with a radius of \textit{d/2}, thus the change in \textit{x} of length \textit{d/2} from the center of the tube, describes a right angle triangle with a hypotenuse of the ellipse function (Equation \ref{eq:ellipse}) rearranged for \textit{x} shown in Equation \ref{eq:ellipseX}, is then revolved by 2/${\pi}$ radians.

\begin{equation} \label{eq:ellipseX}
    \begin{aligned}
        \frac{x^2}{a^2} + \frac{z^2}{b^2} = 1 \\
        \frac{x^2}{a^2} = 1 - \frac{z^2}{b^2} \\
        x = a \sqrt{1 - \frac{z^2}{b^2}}
    \end{aligned}
\end{equation} 

Using Equation \ref{eq:ellipseX} and converting it to cylindrical coordinates is shown in Equation \ref{eq:ellipseXparam}. 

\begin{equation} \label{eq:ellipseXparam}
    \begin{aligned}
        x = a \sqrt{1 - \left(\frac{r\cos(\theta)}{b}\right)^2}
    \end{aligned}
\end{equation} 

Using Equation \ref{eq:ellipseXparam} as the lower integration limit for inner most integration which goes until \textit{a}, the upper limit and the end of the tube described in Equation \ref{eq:capInt}.


\begin{equation} \label{eq:capInt}
       V_{CAP} = \iiint\displaylimits_V \,dv = 4\int_{\theta = 0}^{\theta = \frac{2}{\pi}} \int_{r = 0}^{r = \frac{d}{2}} \int_{x = \left(a \sqrt{1 - \left(\frac{r\cos(\theta)}{b}\right)^2}\right)}^{x = a} 1 \,dx\,rdr\,d\theta 
\end{equation} 

The integration is simplified to only solving one quadrant of the volume and then multiplied by four, as there are four quadrants, to ease and speed up the computation for MATLAB.

\subsection{Total Volume} \label{sec:Vtotal}

The total volume of the oil required to fill the shell is calculated using Equation \ref{eq:Vtotal}.

\begin{equation} \label{eq:Vtotal}
    \begin{aligned}
        V_{TOTAL} = V_{SHELL} - 4*V_{TUBE} + 8*V_{CAP}
    \end{aligned}
\end{equation} 

\section{Summary of Results}\label{sec:results}

\subsection{Volume of the Shell} \label{sec:Vshell results}
\begin{equation} \label{eq:Vshell results}
       V_{SHELL} = \iiint\displaylimits_V \,dv = \int_{\theta = 0}^{\theta = 2\pi} \int_{r = 0}^{r = {\left(\sqrt{\frac{(\cos(\theta))^2}{a^2} + \frac{(\sin(\theta))^2}{b^2}}\right)}^{-1}} \int_{y = 0}^{y = L} 1 \,dy\,rdr\,d\theta 
\end{equation} 

\subsection{Volume of the Tube} \label{sec:Vtube results}
\begin{equation} \label{eq:Vtube results}
       V_{TUBE} = \iiint\displaylimits_V \,dv = \int_{\theta = 0}^{\theta = 2\pi} \int_{r = 0}^{r = \frac{d}{2}} \int_{y = -a}^{y = a} 1 \,dy\,rdr\,d\theta 
\end{equation} 

\subsection{Volume of the Cap} \label{sec:Vcap results}
\begin{equation} \label{eq:Vcap results}
       V_{CAP} = \iiint\displaylimits_V \,dv = 4\int_{\theta = 0}^{\theta = \frac{2}{\pi}} \int_{r = 0}^{r = \frac{d}{2}} \int_{x = \left(a \sqrt{1 - \left(\frac{r\cos(\theta)}{b}\right)^2}\right)}^{x = a} 1 \,dx\,rdr\,d\theta 
\end{equation} 

\subsection{Total Volume} \label{sec:Total results}
\begin{equation} \label{eq:Total results}
    \begin{aligned}
        V_{TOTAL} = V_{SHELL} - 4*V_{TUBE} + 8*V_{CAP}
    \end{aligned}
\end{equation} 

\subsection{Summary of Calculations}\label{sec:calculations}

\begin {table}[ht] \label{tab:dimensions}
 \caption{Case Dimensions}  
\vspace{-0.5cm}
    \begin{center}
    \begin{tabular}{||c c c c c||} 
    \hline
    Case \# & \textit{L} [mm] & \textit{a} [mm] & \textit{b} [mm] & \textit{d} [mm]\\ [0.5ex] 
    \hline\hline
    1 & 1000 & 300 & 200 & 80 \\
    \hline
    2 & 1600 & 400 & 500 & 60 \\
    \hline
    \end{tabular}
    \end{center}
    \vspace{-0.3cm}
\end{table}

\begin {table}[ht] \label{tab:results}
 \caption{Volume Calculation Results}  
\vspace{-0.5cm}
    \begin{center}
    \begin{tabular}{||c c c c c||} 
    \hline
    Case \# & $V_{SHELL} [m{^3}]$ & $V_{TUBE} [m{^3}]$ & $V_{CAP} [m{^3}]$ & $V_{TOTAL} [m{^3}]$\\ [0.5ex] 
    \hline\hline
    1 & 0.18849 & 0.00301 & 7.57800e-06 & 0.17649 \\
    \hline
    2 & 1.00530 & 0.00226 & 5.09167e-07 & 0.99626 \\
    \hline
    \end{tabular}
    \end{center}
    \vspace{-0.3cm}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BACK MATTER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \backmatter will make the \section commands ignore their numbering,
\backmatter

% Here, we insert a References section, which will be formatted properly.
% The list of works you have referenced should be in FILENAME.bib,
% which will be workreport-sample.bib, if you use the command below.
%
% Note, you will need to process the document in a certain order.  First,
% run LaTeX.  The % first pass will allow LaTeX to build a list of 
% references, it may % emit warning messages such as:
%   LaTeX Warning: Reference `app:gnugpl' on page 4 undefined on input line 277.
%   LaTeX Warning: There were undefined references.
% This is normal.  Now you run BiBTeX in order to generate the proper
% layout for the references.  After this, you run LaTeX once more.
%\bibliography{uw-wkrpt-bib}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% APPENDICES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \appendix will reset \section numbers and turn them into letters.
%%
%% Don't forget to refer to all your appendices in the main report.
\appendix


\section{MATLAB Code}\label{app:code}
\lstinputlisting{images/main.m} 
    
\end{document}
