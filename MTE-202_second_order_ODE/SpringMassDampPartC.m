clear all;
% Denifition of Variables
F0 = 1;
m = 1;
Wn = 1;
Z = 0.4;
Omega = [0.1, 0.5, 1, 2, 10];

%solving the differential equation of mass-spring-damper system with
%different omega values
for i = 1:5 
    syms x(t)
    Dx = diff(x);
    x(t) = dsolve(diff(x,2) + (2*Wn)*(Z*Dx) + (Wn^2)*x == (F0/m)*sin(Omega(i)*t),...
        x(0)==0, Dx(0)==0)
    %plotting spring-mass-damper system response
    figure(1)
    set(gca,'FontSize',12);
    plot(0:0.1:25,x(0:0.1:25));
    hold on
    clear x(t)
end

%labeling the graph
title('Spring Mass Damper Response System Displacement vs Time')
ylabel('Displacement (m)')
xlabel('Time (s)')
legend('\Omega = 0.1','\Omega = 0.5','\Omega = 1','\Omega = 2','\Omega = 10','Location','southwest')
box off
