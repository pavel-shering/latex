\documentclass[leqno]{article} %fleqn
\usepackage{mathtools}
\usepackage{color, graphicx}
\usepackage{authblk}
\usepackage[margin=1in]{geometry}
\usepackage{parskip}
\usepackage{amsmath, amssymb, amsthm}

\usepackage{fancyref}

\setlength{\parindent}{0in}
\usepackage{fancyhdr}
%\newcommand{\tab}[1]{\hspace{.05\textwidth}%\rlap{#1}}
%\fancypagestyle{plain}
%{    
%	\fancyhead[L]{MTE-202}
%    \fancyhead[R]{Pavel Shering \\ 20523043}
%
%}
\title{Project \#2 Second Order ODE Applications} 
\author[1]{Pavel Shering}

\affil[1]{\emph{Department of Mechanical \& Mechatronics Engineering} \\
University of Waterloo}
\date{\today}
\vspace{-2.0cm}

\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}



% New definition of square root:
% it renames \sqrt as \oldsqrt
\let\oldsqrt\sqrt
% it defines the new \sqrt in terms of the old one
\def\sqrt{\mathpalette\DHLhksqrt}
\def\DHLhksqrt#1#2{%
\setbox0=\hbox{$#1\oldsqrt{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}

\begin{document}
\vspace{-4.0cm}
\maketitle
\vspace{-0.5cm}
This report involves modelling vibration of a spring-mass-damper mechanical system through manipulation of one-dimensional motion of mass differential equation \eqref{eq:eq1}. 
\begin{equation} \label{eq:eq1}
mx''+cx'+kx=F(t)
\end{equation}


\section*{Part A}
\vspace{-0.5 cm}
Converting the mass motion differential equation \eqref{eq:eq1} into an equation in terms of natural frequency and damping ratio. \\
\[\def\arraystretch{1.5}
  \begin{array}{l l}
    \omega_n= \sqrt{\frac{k}{m}} \text{- natural frequency vibration without damping} & \quad {x\left(0\right)=x_0}\\
    \zeta=\frac{c}{2m\omega_n} \text{- damping ratio, indicates transient system response} & \quad x'\left(0\right)=x'_0
  \end{array} \]

\begin{equation} \label{eq:eq2}
\begin{aligned}
x''+\frac{c}{m}x'+\frac{k}{m}x&=\frac{F(t)}{m} \mbox{,  \quad  } \frac{k}{m}=\omega_n^2 \\
x''+\frac{c}{m}x'+\omega_n^2x&=\frac{F(t)}{m} \mbox{,  \quad  } \frac{c}{m}=2\zeta\omega_n \\
\mathbf{x''}+\mathbf{2\zeta\omega_nx'}+\mathbf{\omega_n^2x}&=\mathbf{\frac{F(t)}{m}}
\end{aligned}
\end{equation} 

\section*{Part B}
\vspace{-0.5 cm}
\[\def\arraystretch{1.5}
  \begin{array}{l l}
   x''+2\zeta\omega_nx'+\omega_n^2x=\frac{F(t)}{m} & \quad F(t)=F_0sin(\Omega t) \quad \text{ $\Omega$ - frequency of forcing function}\\
    x''+2\zeta\omega_nx'+\omega_n^2x=\frac{F_0}{m}sin(\Omega t) & \quad x\left(0\right)=x'\left(0\right)=0
  \end{array} \]
  
\begin{equation} \label{eq:eq3}
\begin{aligned}
\therefore x(t) = x_c + x_p
\end{aligned}
\end{equation} 

$x_c$ - transient solution, homogeneous, reaches equilibrium due to damping \\
$x_p$ - steady state solution

Steady State Solution
\begin{equation} \label{eq:eq4}
\begin{aligned}
x_c = e^{-\zeta\omega_nt} \left(Acos(\sqrt{1-\zeta^2}\omega_nt) + Bsin(\sqrt{1-\zeta^2}\omega_nt)\right)
\end{aligned}
\end{equation} 
 
Solve for steady state
- assume solution form 
\begin{equation} \label{eq:eq5}
\begin{aligned}
	x_p&=Acos(\Omega t)+Bsin(\Omega t)\\
	& \text{  - assume } \Omega \not= \omega_n \\
& \text{  - sub } x_p \eqref{eq:eq3} \text{ into ODE} \eqref{eq:eq1} \\
	x'_p &= -\Omega Asin(\Omega t)+\Omega Bcos(\Omega t)\\
	&= \Omega\left(- Asin(\Omega t)+Bcos(\Omega t)\right)\\
	x''_p &= \Omega \left(-\Omega Acos(\Omega t)-\Omega Bsin(\Omega t)\right) \\
	&= -\Omega^2\left(Acos(\Omega t)+Bsin(\Omega t)\right)\\
	-\Omega^2\left(Acos(\Omega t)+Bsin(\Omega t)\right) + &2\zeta\omega_n \Omega \left(- Asin(\Omega t)+Bcos(\Omega t)\right) + \omega_n^2(Acos(\Omega t)+Bsin(\Omega t) = \frac{F_0}{m}sin(\Omega t) \\
\end{aligned}	
\end{equation} 

- collect likes and split into sine and cosine components
\begin{equation} \label{eq:eq6}
\begin{aligned}
- \Omega^2 Bsin(\Omega t) - 2\zeta\omega_n\Omega Asin(\Omega t) + \omega_n^2 Bsin(\Omega t) = \frac{F_0}{m}sin(\Omega t) \\
\left( B(\omega_n^2 -\Omega^2) - 2\zeta\omega_n\Omega A\right)sin(\Omega t) = \frac{F_0}{m}sin(\Omega t)
\end{aligned}
\end{equation}

\begin{equation} \label{eq:eq7}
\begin{aligned}
- \Omega^2 Acos(\Omega t) + 2\zeta\omega_n\Omega Bcos(\Omega t) + \omega_n^2 Acos(\Omega t) &= \frac{F_0}{m}sin(\Omega t) \\
\left( A(\omega_n^2 -\Omega^2) + 2\zeta\omega_n\Omega B\right)cos(\Omega t) &= 0
\end{aligned}
\end{equation}

- rearrange cosine \eqref{eq:eq7} to solve for A
\begin{equation} \label{eq:eq8}
\begin{aligned}
	A = \frac{-2\zeta\omega_n\Omega B}{(\omega_n^2 -\Omega^2)}
\end{aligned}
\end{equation}

- sub \eqref{eq:eq8} into \eqref{eq:eq6} to solve for B
\begin{equation} \label{eq:eq9}
\begin{aligned}
	B(\omega_n^2 -\Omega^2) - 2\zeta\omega_n\Omega \frac{\left(-2\zeta\omega_n\Omega B\right) }{(\omega_n^2 -\Omega^2)} = \frac{F_0}{m} \\
	\frac{B [(\omega_n^2 -\Omega^2)^2 + ( 2\zeta\omega_n\Omega )^2 ]}{(\omega_n^2 -\Omega^2)} =\frac{F_0}{m} \\		
	\mathbf{B} = \mathbf{\frac{\frac{F_0}{m} (\omega_n^2 -\Omega^2)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 } } 
\end{aligned}
\end{equation}

- use solution of B \eqref{eq:eq9} to solve for A \eqref{eq:eq8}
\begin{equation} \label{eq:eq10}
\begin{aligned}
	\therefore A &= \frac{\frac{F_0}{m}(\omega_n^2 -\Omega^2) (-2\zeta\omega_n\Omega)}{[(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 ](\omega_n^2 -\Omega^2) }  \\ 	
	\mathbf{A} &= \mathbf{\frac{\frac{F_0}{m} (-2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2}}
\end{aligned}
\end{equation}

\begin{equation} \label{eq:eq11}
\begin{aligned}
	&\therefore \mathbf{x_p} = \mathbf{\frac{\frac{F_0}{m} (-2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2}} \mathbf{cos(\Omega t)} + \mathbf{\frac{\frac{F_0}{m} (\omega_n^2 -\Omega^2)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 } } \mathbf{sin(\Omega t)}
\end{aligned}
\end{equation}

- sub $x_p$ \eqref{eq:eq11} and $x_c$ \eqref{eq:eq4} into \eqref{eq:eq3} and solve for A \& B using initial conditions

\begin{equation} \label{eq:eq12}
\begin{aligned}
	x(t) &= e^{-\zeta\omega_nt} Acos(\sqrt{1-\zeta^2}\omega_nt) + e^{-\zeta\omega_nt} Bsin(\sqrt{1-\zeta^2}\omega_nt) + \\ &\frac{\frac{F_0}{m} (-2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2} cos(\Omega t) + \frac{\frac{F_0}{m} (\omega_n^2 -\Omega^2)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 } sin(\Omega t) \\
x(t) &= 0 \\ 
0 &= e^{-\zeta\omega_n (0)}A(1) + \frac{\frac{F_0}{m} (-2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2} (1) \\
\mathbf{A} &= \mathbf{\frac{\frac{F_0}{m} (2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2} } 
\end{aligned}
\end{equation}

\begin{equation} \label{eq:eq13}
\begin{aligned}
	x'(t) &= 0 \\
	x'(t) &= -\zeta\omega_n  e^{-\zeta\omega_nt} Acos(\sqrt{1-\zeta^2}\omega_nt) - e^{-\zeta\omega_nt}A (\sqrt{1-\zeta^2}\omega_n)sin(\sqrt{1-\zeta^2}\omega_nt) - \\ &\zeta\omega_n  e^{-\zeta\omega_nt} Bsin(\sqrt{1-\zeta^2}\omega_nt) + e^{-\zeta\omega_nt} B(\sqrt{1-\zeta^2}\omega_n)cos(\sqrt{1-\zeta^2}\omega_nt) + \\ &\frac{\frac{F_0}{m} (2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2}\Omega sin(\Omega t) + \frac{\frac{F_0}{m} (\omega_n^2 -\Omega^2)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 } \Omega cos(\Omega t) \\
	0 &=  -\zeta\omega_n (1) A (1) + (1)B(\sqrt{1-\zeta^2}\omega_n)(1) +\frac{\frac{F_0}{m} (\omega_n^2 -\Omega^2) \Omega}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 }  (1) \\
	0 &=  -\zeta\omega_n \frac{\frac{F_0}{m} (2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2} + B(\sqrt{1-\zeta^2}\omega_n) +\frac{\frac{F_0}{m} (\omega_n^2 -\Omega^2) \Omega}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 } \\
	\mathbf{B} &= \mathbf{\frac{\frac{F_0}{m} \Omega }{\sqrt{1-\zeta^2}\omega_n} \left[\frac{(2\zeta^2\omega_n^2) + \Omega^2 - \omega^2}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2}\right]}
\end{aligned}
\end{equation}

- substituting A \& B back into the x(t) \eqref{eq:eq3}  to achieve the final equation for displacement of the system


\begin{equation} \label{eq:eq14}
\begin{aligned}
	\mathbf{x(t)} &= \mathbf{e^{-\zeta\omega_nt} \left[\frac{\frac{F_0}{m} (2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2} \right] cos(\sqrt{1-\zeta^2}\omega_nt)} + \\ &\mathbf{e^{-\zeta\omega_nt} \frac{\frac{F_0}{m} \Omega }{\sqrt{1-\zeta^2}\omega_n} \left[\frac{2\zeta^2\omega_n^2) + \Omega^2 - \omega_n^2}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2}\right] sin(\sqrt{1-\zeta^2}\omega_nt)} + \\ &\mathbf{\frac{\frac{F_0}{m} (-2\zeta\omega_n\Omega)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2} cos(\Omega t)} + \mathbf{\frac{\frac{F_0}{m} (\omega_n^2 -\Omega^2)}{(\omega_n^2 -\Omega^2)^2 + (2\zeta\omega_n\Omega)^2 } sin(\Omega t)} \\
\end{aligned}
\end{equation}

\section*{Part C}
\vspace{-0.5 cm}
MATLAB is used to solve the ODE \eqref{eq:eq1} from Part A  for the following constant values:
\[
\begin{array}{cccc}
(\frac{F_0}{m}) = 1 & \quad \omega_n = 1 & \quad \zeta = 0.4 	& \quad \Omega = [0.1, 0.5, 1, 2, 10] 
\end{array} \]
    

\begin{figure}[ht]
\centering
\vspace{-0.5 cm}
\includegraphics[scale=0.5]{images/SpringMassDampPartC.png}
\vspace{-1.25 cm}
\caption{Overall response of the system for 5 values of forcing function frequency.}
\label{fig:partC}
\end{figure}

Figure \ref{fig:partC} demonstrates maximum displacement behaviour of the mass-damper system (from rest)  at the resonance frequency. This occurs as a result of forcing function constructively interfering with the spring's natural frequency  following the direction of movement for the full period. Damping controls the maximum displacement value and controls the constant max amplitude, since without damping the amplitude of oscillations (displacement) of the mass would increase with time. The forcing functions with frequencies close to the resonance frequency show large displacement values, i.e $\Omega = 0.5$. However the further away from natural frequency the lower the displacement values of the mass-damper system. These frequencies represent the destructive interference of the forcing function against the spring's movement.

\section*{Part D}
\vspace{-0.5 cm}
Consider a simple spring mass system equation with a periodic forcing function based on Dirac delta function. 

\begin{equation} \label{eq:eq15}
x'' + \omega_n^2 x = \frac{F_0}{m} \sum\limits_{n=1}^{\infty} \delta (t - nT) \quad \quad  \quad x(0) = x'(0) = 0 
\end{equation}
\vspace*{-1.5 pt}

- use Laplace Transform to solve for displacement
\vspace{-0.5 pt}
\begin{equation} \label{eq:eq16}
\begin{aligned}
	\mathcal{L} \left\lbrace x'' + \omega_n^2 x \right\rbrace &= \mathcal{L} \left\lbrace  \frac{F_0}{m} \sum\limits_{n=1}^{\infty} \delta (t - nT) \right\rbrace \\
	\mathcal{L} \left\lbrace x'' \right\rbrace + \omega_n^2 X(s) &=  \frac{F_0}{m} \sum\limits_{n=1}^{\infty} e^{-nTs} \\
	s^2 X(s) - sx(0) - x'(0) + \omega_n^2 X(s) &=  \frac{F_0}{m} \sum\limits_{n=1}^{\infty} e^{-nTs} \\
	X(s) (s^2 +\omega_n^2) &= \frac{F_0}{m} \sum\limits_{n=1}^{\infty} e^{-nTs} \\
	X(s) &= \frac{\frac{F_0}{m} \sum\limits_{n=1}^{\infty} e^{-nTs}}{(s^2 +\omega_n^2)}  
\end{aligned}
\end{equation}

- take the inverse Laplace Transform
\vspace{-0.5 pt}
\begin{equation} \label{eq:eq17}
\begin{aligned}
	\mathcal{L}^{-1} \left\lbrace X(s) \right\rbrace &= \mathcal{L}^{-1} \left\lbrace \frac{\frac{F_0}{m} \sum\limits_{n=1}^{\infty} e^{-nTs}}{(s^2 +\omega_n^2)} \right\rbrace \\
	x(t) &= \frac{F_0}{m} \sum\limits_{n=1}^{\infty} \mathcal{L}^{-1} \left\lbrace \frac{e^{-nTs}}{\omega_n}  \bullet \frac{\omega_n}{(s^2 +\omega_n^2)} \right\rbrace \\
	\mathbf{x(t)} &= \mathbf{\frac{F_0}{\omega_n m} \sum\limits_{n=1}^{\infty} \left[ sin (\omega_n(t-nT)) \bullet H(t-nT) \right]}
\end{aligned} 
\end{equation}

\section*{Part E}
\vspace{-0.5 cm}
MATLAB is used to plot the function \eqref{eq:eq17} from Part D  for the following constant values:
\[
\begin{array}{cc}
(\frac{F_0}{m}) = 1 & \quad \omega_n = 1 
\end{array} \]
    

\begin{figure}[ht]
\centering
\vspace{-0.75 cm}
\includegraphics[scale=0.6]{images/SpringMassDampPartE.png}
\vspace{-1.25 cm}
\caption{Overall response of the system  when T = 1 such that the frequency of the pings = $\omega_n$ , and when T = 0.5 such that the frequency of pings = $2 \omega_n$.}
\label{fig:partE}
\end{figure}

Figure \ref{fig:partE} shows the constant increasing amplitude of displacement for (T = 1) the system as the period forcing function matches the natural frequency of the spring and constructively interferes with the mass's movement. Since the system does not contain damping, the amplitude of displacement continues to increase with each period of oscillation. However pings occur at twice the natural frequency, for period forcing function is consistently destructively interfering with the system movement as it sets a max allowable amplitude and returns the system to rest, not allowing displacement in the downward direction.

\section*{Appendix}
\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}
Part C MATLAB Script
\lstinputlisting{SpringMassDampPartC.m}

Part E MATLAB Script
\lstinputlisting{SpringMassDampPartE.m}
\end{document}

